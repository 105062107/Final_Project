var player1_1;
var player1_2;
var player2_1;
var player2_2;

var bulletups = [];
var bulletdowns = [];

var prop1 = [];
var prop2 = [];
var prop1_using = [];
var prop2_using = [];

var Player1_lastTime;
var Player2_lastTime;
var lastTime_createprops1;
var lastTime_createprops2;

var DoublePlayerMusic;

var keyboard;

var player1_1prop;
var player1_2prop;
var player1_haveprop;
var player1prop;

var player2_1prop;
var player2_2prop;
var player2_haveprop;
var player2prop;

var mine3;
var mine4;

var player1propkey;
var player2propkey;

var text1;
var text2;

var textlife1;
var textlife2;

var timeup;

var player1_shootspeedup;
var player2_shootspeedup;

var player1_cant_move;
var player2_cant_move;

var gamereadytoend;

var DoublePlayer3State = {

    preload : function() {
        game.load.image('edge1', 'assets/edge1.png');
        game.load.image('edge2', 'assets/edge2.png');

        game.load.spritesheet('player1', 'assets/playerGreen24x27.png', 24, 27);
        game.load.spritesheet('player2', 'assets/playerRed24x27.png', 24, 27);   
        game.load.spritesheet('player111', 'assets/player1.jpg', 25, 25);
        game.load.spritesheet('player222', 'assets/player2.png', 25, 25);        
        game.load.spritesheet('bulletup', 'assets/bulletup.png', 16, 16);
        game.load.spritesheet('bulletdown', 'assets/bulletdown.png', 16, 16);
        game.load.spritesheet('spritesheet', 'assets/spritesheet.png', 40, 40);
        game.load.spritesheet('spikeup', 'assets/spikeup.png', 40, 19);
        game.load.spritesheet('spikedown', 'assets/spikedown.png', 40, 19);
        game.load.spritesheet('gadget', 'assets/gadget.png', 30, 30);
        game.load.spritesheet('bow', 'assets/Bow.png', 16, 16);
        game.load.spritesheet('fireball', 'assets/FireBall.png', 16, 16);
        game.load.spritesheet('gun', 'assets/Gun.png', 16, 16);
        game.load.spritesheet('lightning', 'assets/Lightning.png', 16, 16);
        game.load.spritesheet('mine', 'assets/Mine.png', 16, 16);
        game.load.spritesheet('rocket', 'assets/rocket.png', 16, 16);
        game.load.spritesheet('shiningball', 'assets/ShiningBall.png', 16, 16);
        game.load.spritesheet('heart', 'assets/heart.png', 16, 16);
        game.load.spritesheet('arrowdown', 'assets/arrowdown.png', 20, 40);
        game.load.spritesheet('arrowup', 'assets/arrowup.png', 20, 40);
        game.load.spritesheet('mine3', 'assets/mine3.png', 32, 32);
        game.load.spritesheet('pixel', 'assets/pixel.jpg', 30, 30);
        game.load.spritesheet('lazerup', 'assets/lazer2.png', 40, 680);
        game.load.spritesheet('lazerdown', 'assets/lazer3.png', 40, 680);
        game.load.image('fire1', 'assets/fire1.png');
        game.load.image('fire2', 'assets/fire2.png');
        game.load.image('fire3', 'assets/fire3.png');
        game.load.image('smoke', 'assets/smoke-puff.png');
        game.load.spritesheet('midline', 'assets/midline.png', 1200, 20);
        game.load.spritesheet('bomb', 'assets/White_square.jpg', 16, 16);
        game.load.spritesheet('brokenheart', 'assets/brokenheart.png', 24, 24);
        game.load.spritesheet('24heart', 'assets/24heart.png', 24, 24);
        game.load.spritesheet('player1win', 'assets/player1win.png', 800, 800);
        game.load.spritesheet('player2win', 'assets/player2win.png', 800, 800);
        game.load.spritesheet('timeup', 'assets/timeup.png', 500, 300);
        game.load.spritesheet('endgame', 'assets/endgame.png', 500, 300);
        
        game.load.audio('DoublePlayerMusic', ['assets/music/DoublePlayerMusic.wav', 'assets/music/DoublePlayerMusic.ogg']);
    },
    
    create : function() {

        Player1_lastTime = 0;
        Player2_lastTime = 0;
        lastTime_createprops1 = 0;
        lastTime_createprops2 = 0;
        bulletups = [];
        bulletdowns = [];
        prop1 = [];
        prop2 = [];
        prop1_using = [];
        prop2_using = [];
        mine3 = [];
        mine4 = [];
        player1_haveprop = 0;
        player2_haveprop = 0;
        player1_shootspeedup = 0;
        player2_shootspeedup = 0;
        player1_cant_move = 0;
        player2_cant_move = 0;
        gamereadytoend = 0;

        DoublePlayerMusic = game.add.audio('DoublePlayerMusic');
        DoublePlayerMusic.play();
        DoublePlayerMusic.loop = true; 

        blockCancel = game.add.audio('blockCancel');
        eatHeart = game.add.audio('eatHeart');
        gunshot = game.add.audio('gunshot');
        gunshot.volume += 5;
        itempick = game.add.audio('itempick');
        hurt = game.add.audio('hurt');
        rocketSound = game.add.audio('rocket');
        lazerSound = game.add.audio('lazer');
        arrowSound = game.add.audio('arrow');
        arrowSound.volume += 5;
        gunreload = game.add.audio('gunreload');
        gunreload.volume += 5;
        throwfire = game.add.audio('throwfire');
        crashsound = game.add.audio('crash');

        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            '1': Phaser.Keyboard.NUMPAD_1,
            '2': Phaser.Keyboard.NUMPAD_2,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D,
            'c': Phaser.Keyboard.C,
            'v': Phaser.Keyboard.V,
        });

        var p1text = game.add.text(1120, 20, 'Player1', { font: '15px Arial', fill: 'red' });
        var p2text = game.add.text(1120, 660, 'Player2', { font: '15px Arial', fill: 'red' });

        this.createBounders();
        this.createPlayer1_1();
        this.createPlayer1_2();
        this.createPlayer2_1();
        this.createPlayer2_2();
        this.createMap1();
        this.createMap2();
        this.createMap3();
        this.createMap4();
        this.createHeart();
        timeup = game.add.sprite(350, 200, 'timeup');
        timeup.visible = false;
        endgame = game.add.sprite(350, 200, 'endgame');
        endgame.visible = false;

        player1_1prop = game.add.sprite(player1_1.body.x,player1_1.body.y,'bomb');
        player1_2prop = game.add.sprite(player1_2.body.x,player1_2.body.y,'bomb');
        player2_1prop = game.add.sprite(player2_1.body.x,player2_1.body.y,'bomb');
        player2_2prop = game.add.sprite(player2_2.body.x,player2_2.body.y,'bomb');
        player1prop = game.add.sprite(1200,250,'');
        player2prop = game.add.sprite(1200,450,'');
        player1propkey = '';
        player2propkey = '';
        game.input.onDown.add(this.detectgamereadytoend, self);
        game.input.onDown.add(this.unpause, self);

        var escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        escKey.onDown.add(this.ESC, this);
        var pKey = game.input.keyboard.addKey(Phaser.Keyboard.P);
        pKey.onDown.add(this.P, this);
        this.zeroKey = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_0);
        this.demicalKey = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_DECIMAL);
    },
    
    update : function() {

        this.physics.arcade.collide(player1_1, [leftWall, rightWall, topWall, downWall, middleWall, middle2Wall]);
        this.physics.arcade.collide(player1_2, [leftWall, rightWall, topWall, downWall, middleWall, middle2Wall]);
        this.physics.arcade.collide(player2_1, [leftWall, rightWall, topWall, downWall, middleWall, middle2Wall]);
        this.physics.arcade.collide(player2_2, [leftWall, rightWall, topWall, downWall, middleWall, middle2Wall]);

        this.physics.arcade.collide([player1_1,player1_2],mine4,this.player1_mine_coleffect);
        this.physics.arcade.collide([player2_1,player2_2],mine3,this.player2_mine_coleffect);

        this.physics.arcade.collide(player1_2, prop2_using , this.player1_prop2_coleffect);
        this.physics.arcade.collide(player2_1, prop1_using , this.player2_prop1_coleffect);

        this.physics.arcade.collide(player1_2, bulletups, this.bulletup_player_coleffect);
        this.physics.arcade.collide(player2_1, bulletdowns, this.bulletdown_player_coleffect);

        this.updatePlayer1();
        this.updatePlayer2();

        this.createBulletup();
        this.updateBulletup();
        this.createBulletdown();
        this.updateBulletdown();

        this.createProps1();
        this.updateProps1();
        this.createProps2();
        this.updateProps2();

        this.Player1_1Prop();
        this.Player1_2Prop();
        this.Player2_1Prop();
        this.Player2_2Prop();

        this.physics.arcade.collide(player1_1, prop1, this.prop1_coleffect);
        this.physics.arcade.collide(player1_2, prop1, this.prop1_coleffect);
        this.physics.arcade.collide(player2_1, prop2, this.prop2_coleffect);
        this.physics.arcade.collide(player2_2, prop2, this.prop2_coleffect);

        //this.updateHeart();

        this.player1_skill();
        this.player2_skill();
        this.update_player1_skill();
        this.update_player2_skill();

    },

    createBounders : function() {
        leftWall = game.add.sprite(0, 0, 'edge2');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        topWall = game.add.sprite(0, 0, 'edge1');
        game.physics.arcade.enable(topWall);
        topWall.body.immovable = true;

        rightWall = game.add.sprite(1180, 0, 'edge2');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        downWall = game.add.sprite(0, 680, 'edge1');
        game.physics.arcade.enable(downWall);
        downWall.body.immovable = true;

        middleWall = game.add.sprite(590, 0, 'edge2');
        game.physics.arcade.enable(middleWall);
        middleWall.body.immovable = true;

        middle2Wall = game.add.sprite(0, 340, 'midline');
        middle2Wall.animations.add('midLine',[0,1,2,3,4],8,true);
        middle2Wall.animations.play('midLine');
        game.physics.arcade.enable(middle2Wall);
        middle2Wall.body.immovable = true;
    },

    createPlayer1_1 : function() {
        player1_1 = game.add.sprite(300, 175, 'player1');
        game.physics.arcade.enable(player1_1);
        player1_1.unbeatableTime = 0;
        player1_1.touchOn = undefined;
        player1_1.animations.add('1leftanim', [6, 7, 8], 8, true);
        player1_1.animations.add('1rightanim', [9, 10, 11], 8, true);
        player1_1.animations.add('1upanim', [3, 4, 5], 8, true);
        player1_1.animations.add('1downanim', [0, 1, 2], 8, true);
    },

    createPlayer1_2 : function() {
        player1_2 = game.add.sprite(900, 175, 'player1');
        game.physics.arcade.enable(player1_2);
        player1_2.life = 3;
        player1_2.unbeatableTime = 0;
        player1_2.touchOn = undefined;
        player1_2.animations.add('1leftanim', [6, 7, 8], 8, true);
        player1_2.animations.add('1rightanim', [9, 10, 11], 8, true);
        player1_2.animations.add('1upanim', [3, 4, 5], 8, true);
        player1_2.animations.add('1downanim', [0, 1, 2], 8, true);
    },
    
    createPlayer2_1 : function() {
        player2_1 = game.add.sprite(300, 525, 'player2');
        game.physics.arcade.enable(player2_1);
        player2_1.life = 3;
        player2_1.unbeatableTime = 0;
        player2_1.touchOn = undefined;
        player2_1.animations.add('2leftanim', [6, 7, 8], 8, true);
        player2_1.animations.add('2rightanim', [9, 10, 11], 8, true);
        player2_1.animations.add('2upanim', [3, 4, 5], 8, true);
        player2_1.animations.add('2downanim', [0, 1, 2], 8, true);
    },

    createPlayer2_2 : function() {
        player2_2 = game.add.sprite(900, 525, 'player2');
        game.physics.arcade.enable(player2_2);
        player2_2.unbeatableTime = 0;
        player2_2.touchOn = undefined;
        player2_2.animations.add('2leftanim', [6, 7, 8], 8, true);
        player2_2.animations.add('2rightanim', [9, 10, 11], 8, true);
        player2_2.animations.add('2upanim', [3, 4, 5], 8, true);
        player2_2.animations.add('2downanim', [0, 1, 2], 8, true);
    },

    createHeart : function () {
        textlife1 = game.add.text(1205, 50, '3', { font: '25px Arial', fill: 'yellow' });
        textlife2 = game.add.text(1205, 650, '3', { font: '25px Arial', fill: 'yellow' });
        heart1 = game.add.sprite(1200, 100, '24heart');
        heart2 = game.add.sprite(1200, 150, '24heart');
        heart3 = game.add.sprite(1200, 200, '24heart');
        heart4 = game.add.sprite(1200, 500, '24heart');
        heart5 = game.add.sprite(1200, 550, '24heart');
        heart6 = game.add.sprite(1200, 600, '24heart');
    },

    updateHeart : function (){
        if(player1_2.life == 3){
            textlife1.setText('3');
            heart1 = game.add.sprite(1200, 100, '24heart');
            heart2 = game.add.sprite(1200, 150, '24heart');
            heart3 = game.add.sprite(1200, 200, '24heart');
        }
        else if(player1_2.life == 2){
            textlife1.setText('2');
            heart1 = game.add.sprite(1200, 100, '24heart');
            heart2 = game.add.sprite(1200, 150, '24heart');
            heart3 = game.add.sprite(1200, 200, 'brokenheart');
        }
        else if(player1_2.life == 1){
            textlife1.setText('1');
            heart1 = game.add.sprite(1200, 100, '24heart');
            heart2 = game.add.sprite(1200, 150, 'brokenheart');
            heart3 = game.add.sprite(1200, 200, 'brokenheart');
        }
        else{
            textlife1.setText('GG');
            heart1 = game.add.sprite(1200, 100, 'brokenheart');
            heart2 = game.add.sprite(1200, 150, 'brokenheart');
            heart3 = game.add.sprite(1200, 200, 'brokenheart');
            this.GameoverPlayer2Win();
        }

        if(player2_1.life == 3){
            textlife2.setText('3');
            heart4 = game.add.sprite(1200, 500, '24heart');
            heart5 = game.add.sprite(1200, 550, '24heart');
            heart6 = game.add.sprite(1200, 600, '24heart');
        }
        else if(player2_1.life == 2){
            textlife2.setText('2');
            heart4 = game.add.sprite(1200, 500, 'brokenheart');
            heart5 = game.add.sprite(1200, 550, '24heart');
            heart6 = game.add.sprite(1200, 600, '24heart');
        }
        else if(player2_1.life == 1){
            textlife2.setText('1');
            heart4 = game.add.sprite(1200, 500, 'brokenheart');
            heart5 = game.add.sprite(1200, 550, 'brokenheart');
            heart6 = game.add.sprite(1200, 600, '24heart');
        }
        else{
            textlife2.setText('GG');
            heart4 = game.add.sprite(1200, 500, 'brokenheart');
            heart5 = game.add.sprite(1200, 550, 'brokenheart');
            heart6 = game.add.sprite(1200, 600, 'brokenheart');
            this.GameoverPlayer1Win();
        }
    },

    createBulletup : function() {
        if(game.time.now > player2_shootspeedup){
            if(keyboard.c.isDown && game.time.now > Player2_lastTime + 150){
                Player2_lastTime = game.time.now;
                var bulletup;
                bulletup = game.add.sprite(player2_2.body.x, player2_2.body.y-20, 'bulletup');
                game.physics.arcade.enable(bulletup);
                bulletups.push(bulletup);
                gunshot.play();
            }
        }
        else {
            if(keyboard.c.isDown && game.time.now > Player2_lastTime + 90){
                Player2_lastTime = game.time.now;
                var bulletup;
                bulletup = game.add.sprite(player2_2.body.x, player2_2.body.y-20, 'bulletup');
                game.physics.arcade.enable(bulletup);
                bulletups.push(bulletup);
                player2_shootspeedup = player2_shootspeedup - 0.1;
                gunshot.play();
            }
        }
    },

    updateBulletup : function() {
        for(var i=0; i<bulletups.length; i++) {
            var bulletup = bulletups[i];
            bulletup.body.velocity.y = -500;
            if(bulletup.body.position.y <= 20) {
                bulletup.destroy();
                bulletups.splice(i, 1);
            }
        }
    },

    createBulletdown : function() {
        if(game.time.now > player1_shootspeedup){
            if(this.zeroKey.isDown && game.time.now > Player1_lastTime + 150){
                gunshot.play();
                Player1_lastTime = game.time.now;
                var bulletdown;
                bulletdown = game.add.sprite(player1_1.body.x, player1_1.body.y + 20, 'bulletdown');
                game.physics.arcade.enable(bulletdown);
                bulletdowns.push(bulletdown);
                
            }
        }
        else {
            if(this.zeroKey.isDown && game.time.now > Player1_lastTime + 90){
                Player1_lastTime = game.time.now;
                var bulletdown;
                bulletdown = game.add.sprite(player1_1.body.x, player1_1.body.y + 20, 'bulletdown');
                game.physics.arcade.enable(bulletdown);
                bulletdowns.push(bulletdown);
                gunshot.play();
            }
        }
    },

    updateBulletdown : function() {
        for(var i=0; i<bulletdowns.length; i++) {
            var bulletdown = bulletdowns[i];
            bulletdown.body.velocity.y = 500;
            if(bulletdown.body.position.y >= 680) {
                bulletdown.destroy();
                bulletdowns.splice(i, 1);
            }
        }
    },

    createProps1 : function () {
        if(game.time.now > lastTime_createprops1 + 3000){
            lastTime_createprops1 = game.time.now;
            var prop;
            var temp = Math.random() * 1;
            var x = Math.random()* 554 + 20 + temp * 590;
            var y = Math.random()* 304 + 20;
            var rand = Math.random()* 100;

            if (rand < 8) {
                prop = game.add.sprite(x, y, 'heart');
            } else if (rand < 22) {
                prop = game.add.sprite(x, y, 'bow');
            } else if (rand < 30) {
                prop = game.add.sprite(x, y, 'fireball');
            } else if (rand < 45) {
                prop = game.add.sprite(x, y, 'gun');
            } else if (rand < 62) {
                prop = game.add.sprite(x, y, 'lightning');
            } else if (rand < 80) {
                prop = game.add.sprite(x, y, 'rocket');
            } else if (rand < 90) {
                prop = game.add.sprite(x, y, 'shiningball');
            } else {
                prop = game.add.sprite(x, y, 'mine');
            }

            game.physics.arcade.enable(prop);
            prop.body.immovable = true;
            prop1.push(prop);
        }
    },

    updateProps1 : function () {
        for(var i=0; i<prop1.length; i++) {
            var prop = prop1[i];
            if(prop1.length >= 10) {
                prop1[0].destroy();
                prop1.splice(0, 1);
            }
        }
    },

    createProps2 : function () {
        if(game.time.now > lastTime_createprops2 + 3000){
            lastTime_createprops2 = game.time.now;
            var prop;
            var temp = Math.random() * 1;
            var x = Math.random()* 554 + 20 + temp * 590;
            var y = Math.random()* 304 + 360;
            var rand = Math.random()* 100;

            if (rand < 8) {
                prop = game.add.sprite(x, y, 'heart');
            } else if (rand < 22) {
                prop = game.add.sprite(x, y, 'bow');
            } else if (rand < 30) {
                prop = game.add.sprite(x, y, 'fireball');
            } else if (rand < 45) {
                prop = game.add.sprite(x, y, 'gun');
            } else if (rand < 62) {
                prop = game.add.sprite(x, y, 'lightning');
            } else if (rand < 80) {
                prop = game.add.sprite(x, y, 'rocket');
            } else if (rand < 90) {
                prop = game.add.sprite(x, y, 'shiningball');
            } else {
                prop = game.add.sprite(x, y, 'mine');
            }

            game.physics.arcade.enable(prop);
            prop.body.immovable = true;
            prop2.push(prop);
        }
    },

    updateProps2 : function () {
        for(var i=0; i<prop2.length; i++) {
            var prop = prop2[i];
            if(prop2.length >= 10) {
                prop2[0].destroy();
                prop2.splice(0, 1);
            }
        }
    },

    updatePlayer1 : function() {
        if(game.time.now > player1_cant_move){
            if(keyboard.left.isDown) {
                player1_1.facingRight = false;
                player1_1.facingBottom = false;
                player1_1.facingLeft = true;
                player1_1.facingTop = false;
                player1_1.animations.play('1leftanim');
                if(keyboard.up.isDown){
                    player1_1.body.velocity.x = -250;
                    player1_1.body.velocity.y = -250;
                }
                else if(keyboard.down.isDown){
                    player1_1.body.velocity.x = -250;
                    player1_1.body.velocity.y = 250;
                }
                else{
                    player1_1.body.velocity.x = -250;
                    player1_1.body.velocity.y = 0;
                }	
            } else if(keyboard.right.isDown) {
                player1_1.facingRight = true;
                player1_1.facingBottom = false;
                player1_1.facingLeft = false;
                player1_1.facingTop = false;
                player1_1.animations.play('1rightanim');
                 if(keyboard.up.isDown){
                    player1_1.body.velocity.x = 250;
                    player1_1.body.velocity.y = -250;
                }
                else if(keyboard.down.isDown){
                    player1_1.body.velocity.x = 250;
                    player1_1.body.velocity.y = 250;
                }
                else{
                    player1_1.body.velocity.x = 250;
                    player1_1.body.velocity.y = 0;
                }
            } else if(keyboard.up.isDown) {
                player1_1.facingRight = false;
                player1_1.facingBottom = false;
                player1_1.facingLeft = false;
                player1_1.facingTop = true;
                player1_1.animations.play('1upanim');
                player1_1.body.velocity.y = -250;
                player1_1.body.velocity.x = 0;
            } else if(keyboard.down.isDown) {
                player1_1.facingRight = false;
                player1_1.facingBottom = true;
                player1_1.facingLeft = false;
                player1_1.facingTop = false;
                player1_1.animations.play('1downanim');
                player1_1.body.velocity.y = 250;
                player1_1.body.velocity.x = 0;
            } else {
                if(player1_1.facingBottom) player1_1.frame = 0;
                else if(player1_1.facingTop) player1_1.frame = 3;
                else if(player1_1.facingLeft) player1_1.frame = 6;
                else player1_1.frame = 9;
                player1_1.body.velocity.x = 0;
                player1_1.body.velocity.y = 0;
            }
        }
        else {
            if(player1_1.facingBottom) player1_1.frame = 0;
            else if(player1_1.facingTop) player1_1.frame = 3;
            else if(player1_1.facingLeft) player1_1.frame = 6;
            else player1_1.frame = 9;
            player1_1.body.velocity.x = 0;
            player1_1.body.velocity.y = 0;
        }
        //this.setPlayerAnimate(player1_1);

        if(game.time.now > player1_cant_move){
            if(keyboard.left.isDown) {
                player1_2.facingRight = false;
                player1_2.facingBottom = false;
                player1_2.facingLeft = true;
                player1_2.facingTop = false;
                player1_2.animations.play('1leftanim');
                if(keyboard.up.isDown){
                    player1_2.body.velocity.x = -250;
                    player1_2.body.velocity.y = -250;
                }
                else if(keyboard.down.isDown){
                    player1_2.body.velocity.x = -250;
                    player1_2.body.velocity.y = 250;
                }
                else{
                    player1_2.body.velocity.x = -250;
                    player1_2.body.velocity.y = 0;
                }	
            } else if(keyboard.right.isDown) {
                player1_2.facingRight = true;
                player1_2.facingBottom = false;
                player1_2.facingLeft = false;
                player1_2.facingTop = false;
                player1_2.animations.play('1rightanim');
                if(keyboard.up.isDown){
                    player1_2.body.velocity.x = 250;
                    player1_2.body.velocity.y = -250;
                }
                else if(keyboard.down.isDown){
                    player1_2.body.velocity.x = 250;
                    player1_2.body.velocity.y = 250;
                }
                else{
                    player1_2.body.velocity.x = 250;
                    player1_2.body.velocity.y = 0;
                }
            } else if(keyboard.up.isDown) {
                player1_2.facingRight = false;
                player1_2.facingBottom = false;
                player1_2.facingLeft = false;
                player1_2.facingTop = true;
                player1_2.animations.play('1upanim');
                player1_2.body.velocity.y = -250;
                player1_2.body.velocity.x = 0;
            } else if(keyboard.down.isDown) {
                player1_2.facingRight = false;
                player1_2.facingBottom = true;
                player1_2.facingLeft = false;
                player1_2.facingTop = false;
                player1_2.animations.play('1downanim');
                player1_2.body.velocity.y = 250;
                player1_2.body.velocity.x = 0;
            } else {
                if(player1_2.facingBottom) player1_2.frame = 0;
                else if(player1_2.facingTop) player1_2.frame = 3;
                else if(player1_2.facingLeft) player1_2.frame = 6;
                else player1_2.frame = 9;
                player1_2.body.velocity.x = 0;
                player1_2.body.velocity.y = 0;
            }
        }
        else {
            if(player1_2.facingBottom) player1_2.frame = 0;
            else if(player1_2.facingTop) player1_2.frame = 3;
            else if(player1_2.facingLeft) player1_2.frame = 6;
            else player1_2.frame = 9;
            player1_2.body.velocity.x = 0;
            player1_2.body.velocity.y = 0;
        }

        //this.setPlayerAnimate(player1_2);
    },

    updatePlayer2 : function() {
        if(game.time.now > player2_cant_move){
            if(keyboard.a.isDown) {
                player2_1.facingRight = false;
                player2_1.facingBottom = false;
                player2_1.facingLeft = true;
                player2_1.facingTop = false;
                player2_1.animations.play('2leftanim');
                if(keyboard.w.isDown){
                    player2_1.body.velocity.x = -250;
                    player2_1.body.velocity.y = -250;
                }
                else if(keyboard.s.isDown){
                    player2_1.body.velocity.x = -250;
                    player2_1.body.velocity.y = 250;
                }
                else{
                    player2_1.body.velocity.x = -250;
                    player2_1.body.velocity.y = 0;
                }	
            } else if(keyboard.d.isDown) {
                player2_1.facingRight = true;
                player2_1.facingBottom = false;
                player2_1.facingLeft = false;
                player2_1.facingTop = false;
                player2_1.animations.play('2rightanim');
                if(keyboard.w.isDown){
                    player2_1.body.velocity.x = 250;
                    player2_1.body.velocity.y = -250;
                }
                else if(keyboard.s.isDown){
                    player2_1.body.velocity.x = 250;
                    player2_1.body.velocity.y = 250;
                }
                else{
                    player2_1.body.velocity.x = 250;
                    player2_1.body.velocity.y = 0;
                }
            } else if(keyboard.w.isDown) {
                player2_1.facingRight = false;
                player2_1.facingBottom = false;
                player2_1.facingLeft = false;
                player2_1.facingTop = true;
                player2_1.animations.play('2upanim');
                player2_1.body.velocity.y = -250;
                player2_1.body.velocity.x = 0;
            } else if(keyboard.s.isDown) {
                player2_1.facingRight = false;
                player2_1.facingBottom = true;
                player2_1.facingLeft = false;
                player2_1.facingTop = false;
                player2_1.animations.play('2downanim');
                player2_1.body.velocity.y = 250;
                player2_1.body.velocity.x = 0;
            } else {
                if(player2_1.facingBottom) player2_1.frame = 0;
                else if(player2_1.facingTop) player2_1.frame = 3;
                else if(player2_1.facingLeft) player2_1.frame = 6;
                else player2_1.frame = 9;
                player2_1.body.velocity.x = 0;
                player2_1.body.velocity.y = 0;
            }
        }
        else{
            if(player2_1.facingBottom) player2_1.frame = 0;
            else if(player2_1.facingTop) player2_1.frame = 3;
            else if(player2_1.facingLeft) player2_1.frame = 6;
            else player2_1.frame = 9;
            player2_1.body.velocity.x = 0;
            player2_1.body.velocity.y = 0;
        }
        //this.setPlayerAnimate(player1_1);

        if(game.time.now > player2_cant_move){
            if(keyboard.a.isDown) {
                player2_2.facingRight = false;
                player2_2.facingBottom = false;
                player2_2.facingLeft = true;
                player2_2.facingTop = false;
                player2_2.animations.play('2leftanim');
                if(keyboard.w.isDown){
                    player2_2.body.velocity.x = -250;
                    player2_2.body.velocity.y = -250;
                }
                else if(keyboard.s.isDown){
                    player2_2.body.velocity.x = -250;
                    player2_2.body.velocity.y = 250;
                }
                else{
                    player2_2.body.velocity.x = -250;
                    player2_2.body.velocity.y = 0;
                }	
            } else if(keyboard.d.isDown) {
                player2_2.facingRight = true;
                player2_2.facingBottom = false;
                player2_2.facingLeft = false;
                player2_2.facingTop = false;
                player2_2.animations.play('2rightanim');
                 if(keyboard.w.isDown){
                    player2_2.body.velocity.x = 250;
                    player2_2.body.velocity.y = -250;
                }
                else if(keyboard.s.isDown){
                    player2_2.body.velocity.x = 250;
                    player2_2.body.velocity.y = 250;
                }
                else{
                    player2_2.body.velocity.x = 250;
                    player2_2.body.velocity.y = 0;
                }
            } else if(keyboard.w.isDown) {
                player2_2.facingRight = false;
                player2_2.facingBottom = false;
                player2_2.facingLeft = false;
                player2_2.facingTop = true;
                player2_2.animations.play('2upanim');
                player2_2.body.velocity.y = -250;
                player2_2.body.velocity.x = 0;
            } else if(keyboard.s.isDown) {
                player2_2.facingRight = false;
                player2_2.facingBottom = true;
                player2_2.facingLeft = false;
                player2_2.facingTop = false;
                player2_2.animations.play('2downanim');
                player2_2.body.velocity.y = 250;
                player2_2.body.velocity.x = 0;
            } else {
                if(player2_2.facingBottom) player2_2.frame = 0;
                else if(player2_2.facingTop) player2_2.frame = 3;
                else if(player2_2.facingLeft) player2_2.frame = 6;
                else player2_2.frame = 9;
                player2_2.body.velocity.x = 0;
                player2_2.body.velocity.y = 0;
            }
        }
        else{
            if(player2_2.facingBottom) player2_2.frame = 0;
            else if(player2_2.facingTop) player2_2.frame = 3;
            else if(player2_2.facingLeft) player2_2.frame = 6;
            else player2_2.frame = 9;
            player2_2.body.velocity.x = 0;
            player2_2.body.velocity.y = 0;
        }


        //this.setPlayerAnimate(player1_2);
    },

    player1_skill : function() {
        if(this.demicalKey.isDown && player1_haveprop){
            player1prop.destroy();
            if(player1propkey == 'rocket'){
                rocketSound.play();
                var rocket;
                rocket = game.add.sprite(player1_1.body.x, player1_1.body.y+20, 'rocket');
                game.physics.arcade.enable(rocket);
                prop1_using.push(rocket);
                player1propkey = 'bomb';
                player1_haveprop = 0; 
            }
            if(player1propkey == 'shiningball'){
                player2_cant_move = game.time.now + 1000;
                player1propkey = 'bomb';
                player1_haveprop = 0;
            }
            if(player1propkey == 'heart'){
                eatHeart.play();
                if(player1_2.life<3) {player1_2.life++; DoublePlayerState.updateHeart();}
                player1propkey = 'bomb';
                player1_haveprop = 0;
            }
            if(player1propkey == 'mine'){ 
                var x = Math.random() *1200;
                var y = Math.random() *350+350;
                var mine1 = game.add.sprite(x, y, 'mine3');
                mine3.push(mine1);
                game.physics.arcade.enable(mine1);
                player1propkey = 'bomb';
                player1_haveprop = 0;
            }
            if(player1propkey == 'lightning'){ 
                lazerdown = game.add.sprite(player1_1.body.x, player1_1.body.y+20, 'lazerdown');
                lazerdown.animations.add('lazer', [2, 0, 1, 2, 3], 16, false);
                lazerdown.play('lazer',null,false,true);
                lazerSound.play();
                if(player2_1.body.x > player1_1.body.x-40 && player2_1.body.x<player1_1.body.x + 40){
                    hurt.play();
                    player2_1.life--;
                    DoublePlayerState.updateHeart();
                }
                game.camera.flash(0xffffff, 300);
                player1propkey = 'bomb';
                player1_haveprop = 0;
            }
            if(player1propkey == 'gun'){
                gunreload.play();
                player1_shootspeedup = game.time.now + 4000;
                player1propkey = 'bomb';
                player1_haveprop = 0;
            }
            if(player1propkey == 'bow'){ 
                arrowSound.play();
                var arrowdown;
                arrowdown = game.add.sprite(player1_1.body.x, player1_1.body.y+20, 'arrowdown');
                game.physics.arcade.enable(arrowdown);
                prop1_using.push(arrowdown);
                player1propkey = 'bomb';
                player1_haveprop = 0;
            }
            if(player1propkey == 'fireball'){ 
                throwfire.play();
                var fire;
                fire = game.add.sprite(player1_1.body.x, player1_1.body.y+20, 'fireball');
                game.physics.arcade.enable(fire);
                prop1_using.push(fire);
                player1propkey = 'bomb';
                player1_haveprop = 0;
            }
            
        }
    },

    player2_skill : function() {
        if(keyboard.v.isDown && player2_haveprop){
            player2prop.destroy();
            if(player2propkey == 'rocket'){
                rocketSound.play();
                var rocket;
                rocket = game.add.sprite(player2_2.body.x, player2_2.body.y-20, 'rocket');
                game.physics.arcade.enable(rocket);
                prop2_using.push(rocket);
                player2propkey = 'bomb';
                player2_haveprop = 0;
                rocket.play();
            }
            if(player2propkey == 'shiningball'){
                player1_cant_move = game.time.now + 1000;
                player2propkey = 'bomb';
                player2_haveprop = 0;
            }
            if(player2propkey == 'heart'){
                eatHeart.play();
                if(player2_1.life<3) {player2_1.life++; DoublePlayerState.updateHeart();}
                else player2_1.life=player2_1.life;
                player2propkey = 'bomb';
                player2_haveprop = 0;
            }
            if(player2propkey == 'mine'){ //待處理
                var x = Math.random() *1200;
                var y = Math.random() *350;
                var mine2 = game.add.sprite(x, y, 'mine3');
                mine4.push(mine2);
                game.physics.arcade.enable(mine2);
                player2propkey = 'bomb';
                player2_haveprop = 0;
            }
            if(player2propkey == 'lightning'){ 
                lazerup = game.add.sprite(player2_2.body.x, player2_2.body.y - 680, 'lazerup');
                lazerup.animations.add('lazer', [2, 0, 1, 2, 3], 16, false);
                lazerup.play('lazer',null,false,true);
                lazerSound.play();
                if(player1_2.body.x >= player2_2.body.x-40 && player1_2.body.x<=player2_2.body.x + 40){
                    hurt.play();
                    player1_2.life--;
                    DoublePlayerState.updateHeart();
                }
                game.camera.flash(0xffffff, 300);
                player2propkey = 'bomb';
                player2_haveprop = 0;
            }
            if(player2propkey == 'gun'){
                gunreload.play();
                player2_shootspeedup = game.time.now + 4000;
                player2propkey = 'bomb';
                player2_haveprop = 0;
            }
            if(player2propkey == 'bow'){ 
                arrowSound.play();
                var arrowup;
                arrowup = game.add.sprite(player2_2.body.x, player2_2.body.y-80, 'arrowup');
                game.physics.arcade.enable(arrowup);
                prop2_using.push(arrowup);
                player2propkey = 'bomb';
                player2_haveprop = 0;
            }
            if(player2propkey == 'fireball'){ 
                throwfire.play();
                var fire;
                fire = game.add.sprite(player2_2.body.x, player2_2.body.y-80, 'fireball');
                game.physics.arcade.enable(fire);
                prop2_using.push(fire);
                player2propkey = 'bomb';
                player2_haveprop = 0;
            }
        }
    },

    Player1_1Prop : function () {
        if(player1propkey == 'bomb'){
            player1_1prop.destroy();
        }
        else{
            player1_1prop.destroy();
            player1_1prop = game.add.sprite(player1_1.body.x - 10, player1_1.body.y + 23, player1propkey);
        }
    },

    Player1_2Prop : function () {
        if(player1propkey == 'bomb'){
            player1_2prop.destroy();
        }
        else{
            player1_2prop.destroy();
            player1_2prop = game.add.sprite(player1_2.body.x - 10, player1_2.body.y + 23, player1propkey);
        }
    },

    Player2_1Prop : function () {
        if(player2propkey == 'bomb'){
            player2_1prop.destroy();
        }
        else{
            player2_1prop.destroy();
            player2_1prop = game.add.sprite(player2_1.body.x - 10, player2_1.body.y + 23, player2propkey);
        }
    },

    Player2_2Prop : function () {
        if(player2propkey == 'bomb'){
            player2_2prop.destroy();
        }
        else{
            player2_2prop.destroy();
            player2_2prop = game.add.sprite(player2_2.body.x - 10, player2_2.body.y + 23, player2propkey);
        }
    },

    Player1Prop : function () {
        if(player1propkey == 'bomb'){
            player1prop.destroy();
        }
        else{
            player1prop.destroy();
            player1prop = game.add.sprite(1200, 250, player1propkey);
        }
    },

    Player2Prop : function () {
        if(player2propkey == 'bomb'){
            player2prop.destroy();
        }
        else{
            player2prop.destroy();
            player2prop = game.add.sprite(1200, 450, player2propkey);
        }
    },

    bulletup_player_coleffect :　function (player, bulletup){
        hurt.play();
        bulletup.kill();
        if(game.time.now > player1_1.unbeatableTime && game.time.now > player1_2.unbeatableTime){
            player1_2.life--;
            DoublePlayerState.updateHeart();
            player1_1.unbeatableTime = game.time.now + 2000;
            player1_2.unbeatableTime = game.time.now + 2000;
        }
    },

    bulletdown_player_coleffect :　function (player, bulletdown){
        hurt.play();
        bulletdown.kill();
        if(game.time.now > player2_1.unbeatableTime && game.time.now > player2_2.unbeatableTime){
            player2_1.life--;
            DoublePlayerState.updateHeart();
            player2_1.unbeatableTime = game.time.now + 2000;
            player2_2.unbeatableTime = game.time.now + 2000;
        }
    },

    bullet_brick_coleffect :　function (player, bulletdown){
        bulletdown.kill();
    },

    gadget_player1_coleffect : function (player, gadget){
        if(game.time.now > player1_1.unbeatableTime && game.time.now > player1_2.unbeatableTime){
            player1_2.life--;
            DoublePlayerState.updateHeart();
            crashsound.play();
            player1_1.unbeatableTime = game.time.now + 2000;
            player1_2.unbeatableTime = game.time.now + 2000;
        }
    },

    gadget_player2_coleffect : function (player, gadget){
        if(game.time.now > player2_1.unbeatableTime && game.time.now > player2_2.unbeatableTime){
            player2_1.life--;
            DoublePlayerState.updateHeart();
            crashsound.play();
            player2_1.unbeatableTime = game.time.now + 2000;
            player2_2.unbeatableTime = game.time.now + 2000;
        }
    },

    prop1_coleffect : function (player, prop){
        itempick.play();
        player1propkey = prop.key;
        prop.destroy();
        player1_haveprop = 1;
        DoublePlayerState.Player1Prop();
    },

    prop2_coleffect : function (player, prop){
        itempick.play();
        player2propkey = prop.key;
        prop.destroy();
        player2_haveprop = 1;
        DoublePlayerState.Player2Prop();
    },

    player1_mine_coleffect : function (player, mine){
        emitter = game.add.emitter(player.x, player.y, 400);
        emitter.makeParticles( [ 'fire1', 'fire2', 'fire3', 'smoke' ] );
        emitter.gravity = 200;
        emitter.setAlpha(1, 0, 3000);
        emitter.setScale(0.8, 0, 0.8, 0, 3000);
        emitter.start(true, 8000, null, 15);
        player1_2.life --;
        mine.destroy();
        DoublePlayerState.updateHeart();
    },

    player2_mine_coleffect : function (player, mine){
        emitter = game.add.emitter(player.x, player.y, 400);
        emitter.makeParticles( [ 'fire1', 'fire2', 'fire3', 'smoke' ] );
        emitter.gravity = 200;
        emitter.setAlpha(1, 0, 3000);
        emitter.setScale(0.8, 0, 0.8, 0, 3000);
        emitter.start(true, 8000, null, 15);
        player2_1.life --;
        mine.destroy();
        DoublePlayerState.updateHeart();
    },

    createMap1 : function() {
    },

    createMap2 : function() {
    },

    createMap3 : function() {    
    },

    createMap4 : function() {
    },

    GameoverPlayer1Win : function (){
        game.sound.stopAll();
        game.add.sprite(200,0, 'player1win');
        endgame.visible = true;
        gamereadytoend = 1;
        game.paused = true;
    },

    GameoverPlayer2Win : function (){
        game.sound.stopAll();
        game.add.sprite(200,0, 'player2win');
        endgame.visible = true;
        gamereadytoend = 1;
        game.paused = true;
    },

    player1_prop2_coleffect : function (player, prop) {
        if(prop.key == 'rocket'){
            prop.kill();
            emitter = game.add.emitter(player1_2.x, player1_2.y, 400);
            emitter.makeParticles( [ 'fire1', 'fire2', 'fire3', 'smoke' ] );
            emitter.gravity = 200;
            emitter.setAlpha(1, 0, 3000);
            emitter.setScale(0.8, 0, 0.8, 0, 3000);
            emitter.start(true, 8000, null, 15);
            if(game.time.now > player1_1.unbeatableTime && game.time.now > player1_2.unbeatableTime){
                hurt.play();
                player1_2.life--;
                DoublePlayerState.updateHeart();
                player1_1.unbeatableTime = game.time.now + 2000;
                player1_2.unbeatableTime = game.time.now + 2000;
            }
        }
        if(prop.key == 'arrowup'){
            prop.kill();
            emitter = game.add.emitter(player1_2.x, player1_2.y, 400);
            emitter.makeParticles( [ 'fire1', 'fire2', 'fire3', 'smoke' ] );
            emitter.gravity = 200;
            emitter.setAlpha(1, 0, 3000);
            emitter.setScale(0.8, 0, 0.8, 0, 3000);
            emitter.start(true, 8000, null, 15);
            if(game.time.now > player1_1.unbeatableTime && game.time.now > player1_2.unbeatableTime){
                hurt.play();
                player1_2.life--;
                DoublePlayerState.updateHeart();
                player1_1.unbeatableTime = game.time.now + 2000;
                player1_2.unbeatableTime = game.time.now + 2000;
            }
        }
        if(prop.key == 'fireball'){
            prop.kill();
            emitter = game.add.emitter(player1_2.x, player1_2.y, 400);
            emitter.makeParticles( [ 'fire1', 'fire2', 'fire3', 'smoke' ] );
            emitter.gravity = 200;
            emitter.setAlpha(1, 0, 3000);
            emitter.setScale(0.8, 0, 0.8, 0, 3000);
            emitter.start(true, 8000, null, 15);

            if(game.time.now > player1_1.unbeatableTime && game.time.now > player1_2.unbeatableTime){
                hurt.play();
                player1_2.life --;
                player1_2.life --;
                DoublePlayerState.updateHeart();
                player1_1.unbeatableTime = game.time.now + 2000;
                player1_2.unbeatableTime = game.time.now + 2000;
            }
        }
    },

    player2_prop1_coleffect : function (player, prop) {
        if(prop.key == 'rocket'){
            prop.kill();
            emitter = game.add.emitter(player2_1.x, player2_1.y, 400);
            emitter.makeParticles( [ 'fire1', 'fire2', 'fire3', 'smoke' ] );
            emitter.gravity = 200;
            emitter.setAlpha(1, 0, 3000);
            emitter.setScale(0.8, 0, 0.8, 0, 3000);
            emitter.start(true, 8000, null, 15);
            if(game.time.now > player2_1.unbeatableTime && game.time.now > player2_2.unbeatableTime){
                hurt.play();
                player2_1.life--;
                DoublePlayerState.updateHeart();
                player2_1.unbeatableTime = game.time.now + 2000;
                player2_2.unbeatableTime = game.time.now + 2000;
            }
        }
        if(prop.key == 'arrowdown'){
            prop.kill();
            emitter = game.add.emitter(player2_1.x, player2_1.y, 400);
            emitter.makeParticles( [ 'fire1', 'fire2', 'fire3', 'smoke' ] );
            emitter.gravity = 200;
            emitter.setAlpha(1, 0, 3000);
            emitter.setScale(0.8, 0, 0.8, 0, 3000);
            emitter.start(true, 8000, null, 15);
            if(game.time.now > player2_1.unbeatableTime && game.time.now > player2_2.unbeatableTime){
                hurt.play();
                player2_1.life--;
                DoublePlayerState.updateHeart();
                player2_1.unbeatableTime = game.time.now + 2000;
                player2_2.unbeatableTime = game.time.now + 2000;
            }
        }
        if(prop.key == 'fireball'){
            prop.kill();

            emitter = game.add.emitter(player2_1.x, player2_1.y, 400);
            emitter.makeParticles( [ 'fire1', 'fire2', 'fire3', 'smoke' ] );
            emitter.gravity = 200;
            emitter.setAlpha(1, 0, 3000);
            emitter.setScale(0.8, 0, 0.8, 0, 3000);
            emitter.start(true, 8000, null, 15);

            if(game.time.now > player2_1.unbeatableTime && game.time.now > player2_2.unbeatableTime){
                hurt.play();
                player2_1.life --;
                player2_1.life --;
                DoublePlayerState.updateHeart();
                player2_1.unbeatableTime = game.time.now + 2000;
                player2_2.unbeatableTime = game.time.now + 2000;
            }
        }
    },

    rocket_brick_coleffect : function (brick, prop) {
        if(prop.key == 'rocket'||prop.key == 'arrowup'||prop.key == 'arrowdown'){
            this.emitter = game.add.emitter(brick.x,brick.y,30)
            this.emitter.makeParticles('pixel');
            this.emitter.setYSpeed(-150, 150);
            this.emitter.setXSpeed(-150, 150);
            this.emitter.gravity = 0;
            this.emitter.start(true, 1500, null, 15);
            blockCancel.play();
            game.camera.shake(0.08, 300);
            brick.kill();
            prop.kill();
        }
        if(prop.key == 'fireball'||prop.key == 'shiningball'){
            prop.kill();
        }
    },

    update_player1_skill: function () {
        for(var i=0; i<prop1_using.length; i++) {
            var prop = prop1_using[i];
            if(prop.key == 'rocket') prop.body.velocity.y = 500;
            if(prop.key == 'arrowdown') {
                var a = player2_1.body.x - prop.body.x;
                var b = player2_1.body.y - prop.body.y;
                prop.body.velocity.y = b * 30000 / (Math.pow(a,2) + Math.pow(b,2));
                prop.body.velocity.x = a * 30000 / (Math.pow(a,2) + Math.pow(b,2));
            }
            if(prop.key == 'fireball') prop.body.velocity.y = 500;
            if(prop.body.position.y >= 680) {
                prop.destroy();
                prop1_using.splice(i, 1);
            }
        }
    },
    
    update_player2_skill: function () {
        for(var i=0; i<prop2_using.length; i++) {
            var prop = prop2_using[i];
            if(prop.key == 'rocket') prop.body.velocity.y = -500;
            if(prop.key == 'arrowup') {
                var a = player1_2.body.x - prop.body.x;
                var b = player1_2.body.y - prop.body.y;
                prop.body.velocity.y = b * 30000 / (Math.pow(a,2) + Math.pow(b,2));
                prop.body.velocity.x = a * 30000 / (Math.pow(a,2) + Math.pow(b,2));
            }
            if(prop.key == 'fireball') prop.body.velocity.y = -500;
            if(prop.body.position.y <= 20) {
                prop.destroy();
                prop2_using.splice(i, 1);
            }
        }
    },

    detectgamereadytoend : function (event) {
        if(gamereadytoend == 1){
            game.paused = false;
            if(keyboard.enter.isDown){
                game.state.start('menu');
            }
        }
    },

    P : function() {
        if(game.paused){
            game.paused = false;
            timeup.visible = false;
        }
        else{
            game.paused = true;
            timeup.visible = true;
        }
    },

    ESC : function() {
        game.sound.stopAll();
        game.state.start('menu');
    },

    unpause : function (event) {
        if(game.paused && gamereadytoend == 0){
            if(event.x > 350 && event.x < 850 && event.y > 200 && event.y < 350 ){
                game.paused = false;
                game.state.start('menu');
            }
            else if(event.x > 350 && event.x < 850 && event.y > 350 && event.y < 550 ){
                timeup.visible = false;
                game.paused = false;
            }
        }
    },

    detectgamereadytoend : function (event) {
        if(game.paused && gamereadytoend == 1){
            if(event.x > 350 && event.x < 850 && event.y > 200 && event.y < 350 ){
                endgame.visible = false;
                game.paused = false;
                game.state.start('DoublePlayer3');
            }
            else if(event.x > 350 && event.x < 850 && event.y > 350 && event.y < 550 ){
                endgame.visible = false;
                game.paused = false;
                game.state.start('menu');
            }
        }
    },

}