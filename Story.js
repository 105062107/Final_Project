var storymenu_number;
var Label1;
var Label2;
var Label3;
var Label4;
var nextpageSound;
var storyimage;
var StoryState = {

    preload: function() {

        game.load.image('background', 'assets/background.png');
        game.load.image('villian', 'assets/villian.jpg');
        game.load.image('twoworld', 'assets/twoworld.jpg');
        game.load.image('solider', 'assets/solider.jpg');
        game.load.image('parallel-dimension', 'assets/parallel-dimension.jpg');
        game.load.audio('nextpage', ['assets/music/nextpage.mp3', 'assets/music/nextpage.mp3']);
    },

    create: function() {

        MenuMusic = game.add.audio('MenuMusic');
        MenuMusic.play();
        MenuMusic.loop = true;

        nextpageSound = game.add.audio('nextpage');

        storymenu_number = 1;
        
        var background = game.add.sprite(0, 0, 'background');

        storyimage = game.add.sprite(200, 50, 'parallel-dimension');

        Label1 = game.add.text(game.width/2 , 600, '尼米亞734年，人類發明了一個震驚全銀河系的技術，\n成功利用火箭穿越了黑洞，並從白洞後發現了與原本的宇宙一模一樣的平行世界。', { font: '30px DFKai-sb', fill: 'yellow' });
        Label1.anchor.setTo(0.5, 0.5);

        Label2 = game.add.text(game.width/2 , 600, '但這種技術，卻被一個肆虐於宇宙的恐怖組織給盯上了。\n他們想要利用這個能力去操縱世界。', { font: '30px DFKai-sb', fill: 'yellow' });
        Label2.anchor.setTo(0.5, 0.5);

        Label3 = game.add.text(game.width/2 , 600, '兩邊的世界是相互映照的。\n一方有任何動靜，在另一方必定會以各種不同的方式受到同樣結果的影響。\n恐怖組織就是想要利用這種效應去控制宇宙。', { font: '30px DFKai-sb', fill: 'yellow' });
        Label3.anchor.setTo(0.5, 0.5);

        Label4 = game.add.text(game.width/2 , 600, '玩家將扮演精英部隊，在這兩個世界中摧毀敵人的陣營，破壞恐怖組織的陰謀。\n喝下政府發放密藥的主角，可以同時使用兩個身體在兩方的世界行動。\n主角將利用這個能力，去擊殺恐怖組織的首領，撕碎他們的陰謀。        ', { font: '30px DFKai-sb', fill: 'yellow' });
        Label4.anchor.setTo(0.5, 0.5);

        Label1.visible = true;
        Label2.visible = false;
        Label3.visible = false;
        Label4.visible = false;

        var rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        var leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        var escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);

        rightKey.onDown.add(this.right, this);
        leftKey.onDown.add(this.left, this);
        escKey.onDown.add(this.esc, this);

    },

    right: function() {
        if(storymenu_number  != 4) {storymenu_number++; nextpageSound.play();}

        if(storymenu_number  == 1) {
            Label1.visible = true;
            Label2.visible = false;
            Label3.visible = false;
            Label4.visible = false;
            storyimage.kill();
            storyimage = game.add.sprite(200, 50, 'parallel-dimension');
        }
        if(storymenu_number  == 2) {
            Label1.visible = false;
            Label2.visible = true;
            Label3.visible = false;
            Label4.visible = false;
            storyimage.kill();
            storyimage = game.add.sprite(200, 50, 'villian');
        }
        if(storymenu_number  == 3) {
            Label1.visible = false;
            Label2.visible = false;
            Label3.visible = true;
            Label4.visible = false;
            storyimage.kill();
            storyimage = game.add.sprite(200, 50, 'twoworld');
        }
        if(storymenu_number  == 4) {
            Label1.visible = false;
            Label2.visible = false;
            Label3.visible = false;
            Label4.visible = true;
            storyimage.kill();
            storyimage = game.add.sprite(200, 50, 'solider');
        }
    },

    left: function() {
        if(storymenu_number != 1) {storymenu_number--; nextpageSound.play();}

        if(storymenu_number  == 1) {
            Label1.visible = true;
            Label2.visible = false;
            Label3.visible = false;
            Label4.visible = false;
            storyimage.kill();
            storyimage = game.add.sprite(200, 50, 'parallel-dimension');
        }
        if(storymenu_number  == 2) {
            Label1.visible = false;
            Label2.visible = true;
            Label3.visible = false;
            Label4.visible = false;
            storyimage.kill();
            storyimage = game.add.sprite(200, 50, 'villian');
        }
        if(storymenu_number  == 3) {
            Label1.visible = false;
            Label2.visible = false;
            Label3.visible = true;
            Label4.visible = false;
            storyimage.kill();
            storyimage = game.add.sprite(200, 50, 'twoworld');
        }
        if(storymenu_number  == 4) {
            Label1.visible = false;
            Label2.visible = false;
            Label3.visible = false;
            Label4.visible = true;
            storyimage.kill();
            storyimage = game.add.sprite(200, 50, 'solider');
        }
    },

    esc: function() {
        game.sound.stopAll();
        game.state.start('menu')
    }

}; 