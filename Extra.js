var Label1_1;
var Label2_1;
var Label2_2;
var Label2_3;
var Label2_4;
var Label2_5;
var Label2_6;
var Label2_7;
var Label3_1;
var Label3_2;
var Label3_3;
var Label3_4;
var Label3_5;
var Label3_6;
var Label3_7;
var Label3_8;
var Label3_9;
var Label4_1;
var Label4_2;
var Label4_3;
var Label4_4;
var extramenu_number;


var ExtraState = {

    preload: function() {

        game.load.image('background', 'assets/background.png');
        game.load.audio('nextpage', ['assets/music/nextpage.mp3', 'assets/music/nextpage.mp3']);
        
    },

    create: function() {

        MenuMusic = game.add.audio('MenuMusic');
        MenuMusic.play();
        MenuMusic.loop = true;

        nextpageSound = game.add.audio('nextpage');

        extramenu_number = 1;
        
        var background = game.add.sprite(0, 0, 'background');

        Label1_1 = game.add.text(game.width/2 , 400, '單人遊戲 \n\n 分成左右兩個地圖 \n\n 左右會同時移動 \n\n 到達目的地即為勝利', { font: '30px DFKai-sb', fill: 'yellow' });
        Label1_1.anchor.setTo(0.5, 0.5);

        Label2_1 = game.add.text(game.width/2 , 50, '雙人遊戲(1) 移動介紹', { font: '30px DFKai-sb', fill: 'yellow' });
        Label2_1.anchor.setTo(0.5, 0.5);

        Label2_2 = game.add.text(350 , 120, '角色1移動:WASD', { font: '30px DFKai-sb', fill: 'yellow' });

        Label2_3 = game.add.text(350 , 180, '角色1攻擊:C', { font: '30px DFKai-sb', fill: 'yellow' });

        Label2_4 = game.add.text(350 , 240, '角色1施放技能:V', { font: '30px DFKai-sb', fill: 'yellow' });

        Label2_5 = game.add.text(350 , 300, '角色2移動:上下左右', { font: '30px DFKai-sb', fill: 'yellow' });

        Label2_6 = game.add.text(350 , 360, '角色2攻擊:0', { font: '30px DFKai-sb', fill: 'yellow' });

        Label2_7 = game.add.text(350 , 420, '角色1施放技能:.', { font: '30px DFKai-sb', fill: 'yellow' });

        Label3_1 = game.add.text(game.width/2 , 50, '雙人遊戲(2) 技能介紹', { font: '30px DFKai-sb', fill: 'yellow' });
        Label3_1.anchor.setTo(0.5, 0.5);

        Label3_2 = game.add.text(300 , 120, '心心 : 增加1基本生命', { font: '30px DFKai-sb', fill: 'yellow' });

        Label3_3 = game.add.text(300 , 180, '弓 : 可以無視障礙物進行攻擊', { font: '30px DFKai-sb', fill: 'yellow' });

        Label3_4 = game.add.text(300 , 240, '火球 : 可以一次對敵人造成2點傷害', { font: '30px DFKai-sb', fill: 'yellow' });

        Label3_5 = game.add.text(300 , 300, '槍 : 短暫時間增加射擊速度', { font: '30px DFKai-sb', fill: 'yellow' });

        Label3_6 = game.add.text(300 , 360, '閃電 : 對前方一直線的敵人造成傷害，可以無視障礙物', { font: '30px DFKai-sb', fill: 'yellow' });

        Label3_7 = game.add.text(300 , 420, '火箭 : 可以射穿障礙物，打到人也會有傷害', { font: '30px DFKai-sb', fill: 'yellow' });

        Label3_8 = game.add.text(300 , 480, '閃光球 : 短暫使敵人無法移動', { font: '30px DFKai-sb', fill: 'yellow' });

        Label3_9 = game.add.text(300 , 540, '地雷 : 可以在敵方地圖位置設下地雷，敵人踩到會扣血', { font: '30px DFKai-sb', fill: 'yellow' });

        Label4_1 = game.add.text(game.width/2 , 50, '雙人遊戲(3) 基本規則', { font: '30px DFKai-sb', fill: 'yellow' });
        Label4_1.anchor.setTo(0.5, 0.5);

        Label4_2 = game.add.text(350 , 120, '1 開場雙方各有3命', { font: '30px DFKai-sb', fill: 'yellow' });

        Label4_3 = game.add.text(350 , 180, '2 被傷害到後會短暫無敵', { font: '30px DFKai-sb', fill: 'yellow' });

        Label4_4 = game.add.text(350 , 240, '3 碰到齒輪會扣血', { font: '30px DFKai-sb', fill: 'yellow' });

        Label1_1.visible = true;
        Label2_1.visible = false;
        Label2_2.visible = false;
        Label2_3.visible = false;
        Label2_4.visible = false;
        Label2_5.visible = false;
        Label2_6.visible = false;
        Label2_7.visible = false;
        Label3_1.visible = false;
        Label3_2.visible = false;
        Label3_3.visible = false;
        Label3_4.visible = false;
        Label3_5.visible = false;
        Label3_6.visible = false;
        Label3_7.visible = false;
        Label3_8.visible = false;
        Label3_9.visible = false;
        Label4_1.visible = false;
        Label4_2.visible = false;
        Label4_3.visible = false;
        Label4_4.visible = false;


        var rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        var leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        var escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);

        rightKey.onDown.add(this.right, this);
        leftKey.onDown.add(this.left, this);
        escKey.onDown.add(this.esc, this);

    },

    right: function() {
        if(extramenu_number  != 4) {extramenu_number++; nextpageSound.play();}

        if(extramenu_number  == 1) {
            Label1_1.visible = true;
            Label2_1.visible = false;
            Label2_2.visible = false;
            Label2_3.visible = false;
            Label2_4.visible = false;
            Label2_5.visible = false;
            Label2_6.visible = false;
            Label2_7.visible = false;
            Label3_1.visible = false;
            Label3_2.visible = false;
            Label3_3.visible = false;
            Label3_4.visible = false;
            Label3_5.visible = false;
            Label3_6.visible = false;
            Label3_7.visible = false;
            Label3_8.visible = false;
            Label3_9.visible = false;
            Label4_1.visible = false;
            Label4_2.visible = false;
            Label4_3.visible = false;
            Label4_4.visible = false;
        }
        if(extramenu_number  == 2) {
            Label1_1.visible = false;
            Label2_1.visible = true;
            Label2_2.visible = true;
            Label2_3.visible = true;
            Label2_4.visible = true;
            Label2_5.visible = true;
            Label2_6.visible = true;
            Label2_7.visible = true;
            Label3_1.visible = false;
            Label3_2.visible = false;
            Label3_3.visible = false;
            Label3_4.visible = false;
            Label3_5.visible = false;
            Label3_6.visible = false;
            Label3_7.visible = false;
            Label3_8.visible = false;
            Label3_9.visible = false;
            Label4_1.visible = false;
            Label4_2.visible = false;
            Label4_3.visible = false;
            Label4_4.visible = false;
        }
        if(extramenu_number  == 3) {
            Label1_1.visible = false;
            Label2_1.visible = false;
            Label2_2.visible = false;
            Label2_3.visible = false;
            Label2_4.visible = false;
            Label2_5.visible = false;
            Label2_6.visible = false;
            Label2_7.visible = false;
            Label3_1.visible = true;
            Label3_2.visible = true;
            Label3_3.visible = true;
            Label3_4.visible = true;
            Label3_5.visible = true;
            Label3_6.visible = true;
            Label3_7.visible = true;
            Label3_8.visible = true;
            Label3_9.visible = true;
            Label4_1.visible = false;
            Label4_2.visible = false;
            Label4_3.visible = false;
            Label4_4.visible = false;
        }
        if(extramenu_number  == 4) {
            Label1_1.visible = false;
            Label2_1.visible = false;
            Label2_2.visible = false;
            Label2_3.visible = false;
            Label2_4.visible = false;
            Label2_5.visible = false;
            Label2_6.visible = false;
            Label2_7.visible = false;
            Label3_1.visible = false;
            Label3_2.visible = false;
            Label3_3.visible = false;
            Label3_4.visible = false;
            Label3_5.visible = false;
            Label3_6.visible = false;
            Label3_7.visible = false;
            Label3_8.visible = false;
            Label3_9.visible = false;
            Label4_1.visible = true;
            Label4_2.visible = true;
            Label4_3.visible = true;
            Label4_4.visible = true;
        }
    },

    left: function() {
        if(extramenu_number != 1) {extramenu_number--; nextpageSound.play();}

        if(extramenu_number  == 1) {
            Label1_1.visible = true;
            Label2_1.visible = false;
            Label2_2.visible = false;
            Label2_3.visible = false;
            Label2_4.visible = false;
            Label2_5.visible = false;
            Label2_6.visible = false;
            Label2_7.visible = false;
            Label3_1.visible = false;
            Label3_2.visible = false;
            Label3_3.visible = false;
            Label3_4.visible = false;
            Label3_5.visible = false;
            Label3_6.visible = false;
            Label3_7.visible = false;
            Label3_8.visible = false;
            Label3_9.visible = false;
            Label4_1.visible = false;
            Label4_2.visible = false;
            Label4_3.visible = false;
            Label4_4.visible = false;
        }
        if(extramenu_number  == 2) {
            Label1_1.visible = false;
            Label2_1.visible = true;
            Label2_2.visible = true;
            Label2_3.visible = true;
            Label2_4.visible = true;
            Label2_5.visible = true;
            Label2_6.visible = true;
            Label2_7.visible = true;
            Label3_1.visible = false;
            Label3_2.visible = false;
            Label3_3.visible = false;
            Label3_4.visible = false;
            Label3_5.visible = false;
            Label3_6.visible = false;
            Label3_7.visible = false;
            Label3_8.visible = false;
            Label3_9.visible = false;
            Label4_1.visible = false;
            Label4_2.visible = false;
            Label4_3.visible = false;
            Label4_4.visible = false;
        }
        if(extramenu_number  == 3) {
            Label1_1.visible = false;
            Label2_1.visible = false;
            Label2_2.visible = false;
            Label2_3.visible = false;
            Label2_4.visible = false;
            Label2_5.visible = false;
            Label2_6.visible = false;
            Label2_7.visible = false;
            Label3_1.visible = true;
            Label3_2.visible = true;
            Label3_3.visible = true;
            Label3_4.visible = true;
            Label3_5.visible = true;
            Label3_6.visible = true;
            Label3_7.visible = true;
            Label3_8.visible = true;
            Label3_9.visible = true;
            Label4_1.visible = false;
            Label4_2.visible = false;
            Label4_3.visible = false;
            Label4_4.visible = false;
        }
        if(extramenu_number  == 4) {
            Label1_1.visible = false;
            Label2_1.visible = false;
            Label2_2.visible = false;
            Label2_3.visible = false;
            Label2_4.visible = false;
            Label2_5.visible = false;
            Label2_6.visible = false;
            Label2_7.visible = false;
            Label3_1.visible = false;
            Label3_2.visible = false;
            Label3_3.visible = false;
            Label3_4.visible = false;
            Label3_5.visible = false;
            Label3_6.visible = false;
            Label3_7.visible = false;
            Label3_8.visible = false;
            Label3_9.visible = false;
            Label4_1.visible = true;
            Label4_2.visible = true;
            Label4_3.visible = true;
            Label4_4.visible = true;
        }
    },

    esc: function() {
        game.sound.stopAll();
        game.state.start('menu')
    }

}; 