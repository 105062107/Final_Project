var menu_number = 1;
var stmeu;
var selectSound;
var MenuMusic;

var OnlineModeMenuState = {

    preload: function() {

        game.load.image('startmenu_index', 'assets/menunum.png');
        game.load.image('background', 'assets/background.png');

        game.load.audio('select', ['assets/music/select.wav', 'assets/music/select.ogg']);
        game.load.audio('MenuMusic', ['assets/music/MenuMusic.wav', 'assets/music/MenuMusic.ogg']);
        game.load.audio('blockCancel', 'assets/music/blockcancel.wav');
        game.load.audio('eatHeart', 'assets/music/eatheart.ogg');
        game.load.audio('gunshot', 'assets/music/gunshot.wav');
        game.load.audio('itempick', 'assets/music/itempick.wav');
        game.load.audio('hurt', 'assets/music/hurt.wav');
        game.load.audio('rocket', 'assets/music/rocket.wav');
        game.load.audio('lazer', 'assets/music/strange explosion2.wav');
        game.load.audio('arrow', 'assets/music/arrow.mp3');
        game.load.audio('gunreload', 'assets/music/gunreload.mp3');
        game.load.audio('throwfire', 'assets/music/throwfire.mp3');
        game.load.audio('crash', 'assets/music/crash.mp3');
        //game.load.audio('blockCancel', 'assets/blockcancel.wav');
    },

    create: function() {

        MenuMusic = game.add.audio('MenuMusic');
        MenuMusic.play();
        MenuMusic.loop = true;

        selectSound = game.add.audio('select');

        menu_number = 1;
        
        var background = game.add.sprite(0, 0, 'background');

        stmeu = game.add.sprite(450, 327, 'startmenu_index');

        var Single_Player_Label = game.add.text(game.width/2 , 350, '1P', { font: '25px Arial', fill: '#ff0000' });
        Single_Player_Label.anchor.setTo(0.5, 0.5);

        var Double_Player_Label = game.add.text(game.width/2, 410, '2P', { font: '25px Arial', fill: '#ff0000' });
        Double_Player_Label.anchor.setTo(0.5, 0.5);

        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        var escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);

        upKey.onDown.add(this.up, this);
        downKey.onDown.add(this.down, this);
        enterKey.onDown.add(this.enter, this);
        escKey.onDown.add(this.esc, this);

    },

    up: function() {
        selectSound.play();
        if(menu_number != 1) menu_number--;
        stmeu.kill();

        if(menu_number == 1) stmeu = game.add.sprite(450, 327, 'startmenu_index');
    },

    down: function() {
        selectSound.play();
        if(menu_number != 5) menu_number++;
        stmeu.kill();

        if(menu_number == 2) stmeu = game.add.sprite(450, 387, 'startmenu_index');
    },

    enter: function() {
        game.sound.stopAll();

        if(menu_number == 1) game.state.start('OnlineModeMap1Gamer1P');
        if(menu_number == 2) game.state.start('OnlineModeMap1Gamer2P');
    },

    esc: function() {
        game.sound.stopAll();
        game.state.start('menu');
    },

}; 