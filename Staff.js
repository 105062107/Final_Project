var StaffState = {

    create: function() {

        var background = game.add.sprite(0, 0, 'background');

        var title_Label = game.add.text(game.width/2 , 100, 'Staff', { font: '30px Arial', fill: '#ff0000' });
        title_Label.anchor.setTo(0.5, 0.5);

        var Person1 = game.add.text(game.width/2, 300, '羅士鈞', { font: '40px Arial', fill: '#ff0000' });
        Person1.anchor.setTo(0.5, 0.5);

        var Person2 = game.add.text(game.width/2, 400, '高緯博', { font: '40px Arial', fill: '#ff0000' });
        Person2.anchor.setTo(0.5, 0.5);

        var Person3 = game.add.text(game.width/2, 500, '黃浩軒', { font: '40px Arial', fill: '#ff0000' });
        Person3.anchor.setTo(0.5, 0.5);

        var ESCKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);

        ESCKey.onDown.add(this.ESC, this);

    },

    ESC: function(){
        game.state.start("menu");
    }
}; 