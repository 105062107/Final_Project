var menu_number = 1;
var stmeu;
var selectSound;
var MenuMusic;

var menuState = {

    preload: function() {

        game.load.image('startmenu_index', 'assets/menunum.png');
        game.load.image('background', 'assets/background.png');

        game.load.audio('select', ['assets/music/select.wav', 'assets/music/select.ogg']);
        game.load.audio('MenuMusic', ['assets/music/MenuMusic.wav', 'assets/music/MenuMusic.ogg']);
        game.load.audio('blockCancel', 'assets/music/blockcancel.wav');
        game.load.audio('eatHeart', 'assets/music/eatheart.ogg');
        game.load.audio('gunshot', 'assets/music/gunshot.wav');
        game.load.audio('itempick', 'assets/music/itempick.wav');
        game.load.audio('hurt', 'assets/music/hurt.wav');
        game.load.audio('rocket', 'assets/music/rocket.wav');
        game.load.audio('lazer', 'assets/music/strange explosion2.wav');
        game.load.audio('arrow', 'assets/music/arrow.mp3');
        game.load.audio('gunreload', 'assets/music/gunreload.mp3');
        game.load.audio('throwfire', 'assets/music/throwfire.mp3');
        game.load.audio('crash', 'assets/music/crash.mp3');
        //game.load.audio('blockCancel', 'assets/blockcancel.wav');
    },

    create: function() {

        MenuMusic = game.add.audio('MenuMusic');
        MenuMusic.play();
        MenuMusic.loop = true;

        selectSound = game.add.audio('select');

        menu_number = 1;
        
        var background = game.add.sprite(0, 0, 'background');

        stmeu = game.add.sprite(450, 267, 'startmenu_index');

        var Single_Player1_Label = game.add.text(game.width/2 , 290, 'VS電腦', { font: '25px Arial', fill: '#ff0000' });
        Single_Player1_Label.anchor.setTo(0.5, 0.5);

        var Single_Player_Label = game.add.text(game.width/2 , 350, '單人遊戲', { font: '25px Arial', fill: '#ff0000' });
        Single_Player_Label.anchor.setTo(0.5, 0.5);

        var Double_Player_Label = game.add.text(game.width/2, 410, '雙人遊戲', { font: '25px Arial', fill: '#ff0000' });
        Double_Player_Label.anchor.setTo(0.5, 0.5);

        var Story_Label = game.add.text(game.width/2, 470, '故事背景', { font: '25px Arial', fill: '#ff0000' });
        Story_Label.anchor.setTo(0.5, 0.5);

        var Staff_Label = game.add.text(game.width/2, 530, 'Staff', { font: '25px Arial', fill: '#ff0000' });
        Staff_Label.anchor.setTo(0.5, 0.5);
        
        var Settings_Label = game.add.text(game.width/2, 590, '遊戲設定', { font: '25px Arial', fill: '#ff0000' });
        Settings_Label.anchor.setTo(0.5, 0.5);

        var Online_Label = game.add.text(game.width/2, 650, '線上模式', { font: '25px Arial', fill: '#ff0000' });
        Online_Label.anchor.setTo(0.5, 0.5);

        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        upKey.onDown.add(this.up, this);
        downKey.onDown.add(this.down, this);
        enterKey.onDown.add(this.enter, this);

    },

    up: function() {
        selectSound.play();
        if(menu_number != 1) menu_number--;
        stmeu.kill();

        if(menu_number == 1) stmeu = game.add.sprite(450, 267, 'startmenu_index');
        if(menu_number == 2) stmeu = game.add.sprite(450, 327, 'startmenu_index');
        if(menu_number == 3) stmeu = game.add.sprite(450, 387, 'startmenu_index');
        if(menu_number == 4) stmeu = game.add.sprite(450, 447, 'startmenu_index');
        if(menu_number == 5) stmeu = game.add.sprite(450, 507, 'startmenu_index');
        if(menu_number == 6) stmeu = game.add.sprite(450, 567, 'startmenu_index');
    },

    down: function() {
        selectSound.play();
        if(menu_number != 7) menu_number++;
        stmeu.kill();

        if(menu_number == 2) stmeu = game.add.sprite(450, 327, 'startmenu_index');
        if(menu_number == 3) stmeu = game.add.sprite(450, 387, 'startmenu_index');
        if(menu_number == 4) stmeu = game.add.sprite(450, 447, 'startmenu_index');
        if(menu_number == 5) stmeu = game.add.sprite(450, 507, 'startmenu_index');
        if(menu_number == 6) stmeu = game.add.sprite(450, 567, 'startmenu_index');
        if(menu_number == 7) stmeu = game.add.sprite(450, 627, 'startmenu_index');
    },

    enter: function() {
        game.sound.stopAll();

        if(menu_number == 1) game.state.start('CVP');
        if(menu_number == 2) game.state.start('SinglePlayer');
        if(menu_number == 3) game.state.start('DoublePlayerMenu');
        if(menu_number == 4) game.state.start('Story');
        if(menu_number == 5) game.state.start('Staff');
        if(menu_number == 6) game.state.start('Extra');
        if(menu_number == 7) game.state.start('OnlineModeMenu');
    },

}; 