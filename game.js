var game = new Phaser.Game(1240, 700, Phaser.AUTO, 'canvas');

game.state.add('menu',menuState);
game.state.add('CVP',CVPState);
game.state.add('SinglePlayer',SinglePlayerState);
game.state.add('DoublePlayerMenu',DoublePlayerMenuState);
game.state.add('DoublePlayer',DoublePlayerState);
game.state.add('DoublePlayer2',DoublePlayer2State);
game.state.add('DoublePlayer3',DoublePlayer3State);
game.state.add('DoublePlayer4',DoublePlayer4State);
game.state.add('DoublePlayer5',DoublePlayer5State);
game.state.add('Story',StoryState);
game.state.add('Staff',StaffState);
game.state.add('Extra',ExtraState);
game.state.add('OnlineModeMenu',OnlineModeMenuState);
game.state.add('OnlineModeMap1Gamer1P',OnlineModeMap1Gamer1PState);
game.state.add('OnlineModeMap1Gamer2P',OnlineModeMap1Gamer2PState);

game.state.start('menu');
