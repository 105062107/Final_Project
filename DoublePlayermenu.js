var DoublePlayerMenuState = {

    preload: function() {
    },

    create: function() {

        MenuMusic = game.add.audio('MenuMusic');
        MenuMusic.play();
        MenuMusic.loop = true;

        selectSound = game.add.audio('select');

        menu_number = 1;
        
        var background = game.add.sprite(0, 0, 'background');

        stmeu = game.add.sprite(450, 327, 'startmenu_index');

        var Single_Player_Label = game.add.text(game.width/2 , 350, '地圖1', { font: '25px Arial', fill: '#ff0000' });
        Single_Player_Label.anchor.setTo(0.5, 0.5);

        var Double_Player_Label1 = game.add.text(game.width/2, 410, '地圖2', { font: '25px Arial', fill: '#ff0000' });
        Double_Player_Label1.anchor.setTo(0.5, 0.5);

        var Double_Player_Label2 = game.add.text(game.width/2, 470, '地圖3', { font: '25px Arial', fill: '#ff0000' });
        Double_Player_Label2.anchor.setTo(0.5, 0.5);

        var Double_Player_Label3 = game.add.text(game.width/2, 530, '地圖4', { font: '25px Arial', fill: '#ff0000' });
        Double_Player_Label3.anchor.setTo(0.5, 0.5);

        var Double_Player_Label4 = game.add.text(game.width/2, 590, '地圖5', { font: '25px Arial', fill: '#ff0000' });
        Double_Player_Label4.anchor.setTo(0.5, 0.5);

        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        var ESCKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);

        ESCKey.onDown.add(this.ESC, this);
        upKey.onDown.add(this.up, this);
        downKey.onDown.add(this.down, this);
        enterKey.onDown.add(this.enter, this);

    },

    up: function() {
        selectSound.play();
        if(menu_number != 1) menu_number--;
        stmeu.kill();

        if(menu_number == 1) stmeu = game.add.sprite(450, 327, 'startmenu_index');
        if(menu_number == 2) stmeu = game.add.sprite(450, 387, 'startmenu_index');
        if(menu_number == 3) stmeu = game.add.sprite(450, 447, 'startmenu_index');
        if(menu_number == 4) stmeu = game.add.sprite(450, 507, 'startmenu_index');
    },

    down: function() {
        selectSound.play();
        if(menu_number != 5) menu_number++;
        stmeu.kill();

        if(menu_number == 2) stmeu = game.add.sprite(450, 387, 'startmenu_index');
        if(menu_number == 3) stmeu = game.add.sprite(450, 447, 'startmenu_index');
        if(menu_number == 4) stmeu = game.add.sprite(450, 507, 'startmenu_index');
        if(menu_number == 5) stmeu = game.add.sprite(450, 567, 'startmenu_index');
    },

    enter: function() {
        game.sound.stopAll();

        if(menu_number == 1) game.state.start('DoublePlayer');
        if(menu_number == 2) game.state.start('DoublePlayer2');
        if(menu_number == 3) game.state.start('DoublePlayer3');
        if(menu_number == 4) game.state.start('DoublePlayer4');
        if(menu_number == 5) game.state.start('DoublePlayer5');
    },

    ESC: function() {
        game.sound.stopAll();
        game.state.start('menu');
    }

}; 