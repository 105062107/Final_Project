var SinglePlayerState ={
    preload:function(){
        game.load.spritesheet('player', 'assets/player1.jpg', 25, 25);//since main stage
        game.load.image('tileset', 'assets/spritesheet.png');
        game.load.tilemap('map', 'assets/map.json', null,Phaser.Tilemap.TILED_JSON);
        game.load.spritesheet('brokenheart', 'assets/brokenheart.png', 24, 24);
        //game.load.spritesheet('24heart', 'assets/24heart.png', 24, 24);
        game.load.spritesheet('24heart', 'assets/24heart_2in1.png', 24, 24);
        game.load.spritesheet('mine','assets/Mine2.png');
        game.load.image('explode','assets/fire1.png');
        game.load.image('smoke','assets/smoke-puff.png');
        game.load.image('blood','assets/blood.png')
        game.load.image('enemy_t','assets/enemy_t.png');
        game.load.image('enemy_hv','assets/enemy_hv.png');
        game.load.image('enemy_map','assets/bomb_hole.png');

        game.load.image('maprocket','assets/spr_missile.png');
        game.load.image('target','assets/target.png');

        game.load.spritesheet('rocket', 'assets/rocket.png', 16, 16);
        game.load.spritesheet('bulletup', 'assets/bulletup.png', 16, 16);

        game.load.spritesheet('tail', 'assets/smoke_strip17.png', 32, 32);
        game.load.spritesheet('explode_sheet', 'assets/smoke_strip24.png', 32, 32);
        
    },
    create:function(){
        //keyboard
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'z': Phaser.Keyboard.Z,
        });
        
        //map
        this.map = game.add.tilemap('map'); // Create the tilemap
        // Add the tileset to the map
        this.map.addTilesetImage('tileset');
        // Create the layer by specifying the name of the Tiled layer
        this.layer = this.map.createLayer('Layer1');
        this.layer2 = this.map.createLayer('Layer2');
        // Set the world size to match the size of the layer
        this.layer.resizeWorld();
        //this.map.setCollisionBetween(1, 100,true, this.layer,true);
        this.map.setCollision(111);
        // Create the layer of objects
        //this.objlayer = this.map.createLayer('Object1');
        //player
        this.createPlayer();
        this.createPlayer_alias();
        //player heart
        this.createHeart();
        this.createItems();
        this.createEnemies();
    },
    update:function(){
        this.playerupdate (this.player);
        this.playerupdate (player_alias);
        //this.game.debug.body(this.player, 'rgba(255,0,0,0.5)');
        this.collide(this.player,this.layer);
        this.collide(player_alias,this.layer);
        game.physics.arcade.overlap(player_alias, this.mines, this.step_on_mine, null, this);
        game.physics.arcade.overlap(player_alias, this.heal_packs, this.step_on_heal_pack, null, this);
        if(keyboard.z.isDown){
            this.playerfire();
        }
        game.physics.arcade.collide(this.enemy_ts,this.player);
        game.physics.arcade.collide(this.enemy_hs,this.player);
        game.physics.arcade.collide(this.enemy_vs,this.player);
        game.physics.arcade.collide(this.enemy_maps,this.player);
        game.physics.arcade.collide(this.enemy_maps,this.enemy_vs);
        game.physics.arcade.collide(this.enemy_maps,this.enemy_hs);
        this.enemy_tupdate();
        this.bulletupdate();
        this.rocket_to_drop();
        this.target_to_alert();
        this.heartupdate();
        this.check_game_over();
    },
    createItems: function() {
        //create mines
        this.mines = this.game.add.group();
        this.mines.enableBody = true;
        var mine;    
        this.map.createFromObjects('Object1', 106, 'mine', 0, true, false, this.mines);
        //create heal packs
        this.heal_packs = this.game.add.group();
        this.heal_packs.enableBody = true;
        var heal_pack;    
        this.map.createFromObjects('Object1', 104, '24heart', 0, true, false, this.heal_packs);
      },
    createEnemies: function(){
        this.enemy_ts = this.game.add.group();
        this.enemy_ts.enableBody = true;  
        this.map.createFromObjects('Object1', 6, 'enemy_t', 0, true, false, this.enemy_ts);
        this.enemy_ts.setAll('anchor.x', 0.5);
        this.enemy_ts.setAll('anchor.y', 0.5);
        this.enemy_ts.forEach(element => {
            element.type="rocket";
            element.life = 5;
            element.bulletsnum = 0;
            element.cooldown = 0;
            element.bullets = [];
            game.physics.enable(element);    // Enable Physics
            element.enableBody = true;
            element.body.immovable = true;
        });
            //horizontal turrets
        this.enemy_hs = this.game.add.group();
        this.enemy_hs.enableBody = true;  
        this.map.createFromObjects('Object1', 1, 'enemy_hv', 0, true, false, this.enemy_hs);
        this.enemy_hs.setAll('anchor.x', 0.5);
        this.enemy_hs.setAll('anchor.y', 0.5);
        this.enemy_hs.forEach(element => {
            element.type = 'normal';
            element.life = 3;
            element.cooldown = 0;
            game.physics.enable(element);    // Enable Physics
            element.enableBody = true;
            element.body.immovable = true;
            //element.body.velocity.x = 0;
        });
            //vertical turrets
        this.enemy_vs = this.game.add.group();
        this.enemy_vs.enableBody = true;  
        this.map.createFromObjects('Object1', 5, 'enemy_hv', 0, true, false, this.enemy_vs);
        this.enemy_vs.setAll('anchor.x', 0.5);
        this.enemy_vs.setAll('anchor.y', 0.5);
        this.enemy_vs.forEach(element => {
            element.type = 'normal';
            element.life = 3;
            element.cooldown = 0;
            element.init = element.y;
            game.physics.enable(element);    // Enable Physics
            element.enableBody = true;
            element.body.immovable = true;
        });
        this.enemy_maps = this.game.add.group();
        this.enemy_maps.enableBody = true;  
        this.map.createFromObjects('Object1', 99, 'enemy_map', 0, true, false, this.enemy_maps);
        this.enemy_maps.setAll('anchor.x', 0.5);
        this.enemy_maps.setAll('anchor.y', 0.5);
        this.enemy_maps.forEach(element => {
            element.type = 'map';
            element.life = 10;
            element.cooldown = 0;
            game.physics.enable(element);    // Enable Physics
            element.enableBody = true;
            element.body.immovable = true;
        });
    },
    fireBullet:function(enemy_t_body,type){   
        var X = Math.random();  
        if(type=="rocket"){
            while(game.time.now>enemy_t_body.cooldown+3000){
                var newb = game.add.sprite(enemy_t_body.x,enemy_t_body.y,'rocket');
                game.physics.enable(newb);
                newb.enableBody = true;
                newb.rotation = enemy_t_body.rotation-Math.PI/2;
                newb.type = 'rocket';
                newb.time = game.time.now;
                game.physics.arcade.velocityFromAngle(newb.angle, 200, newb.body.velocity);
                newb.rotation += Math.PI/2;
                game_bullets.push(newb);
                enemy_t_body.cooldown = game.time.now;
            }
        }
        else if(type=="normal_h"){
            //console.log(enemy_t_body.type);
            
            while(game.time.now>enemy_t_body.cooldown+2000){
                var newb = game.add.sprite(enemy_t_body.x,enemy_t_body.y,'bulletup');
                if(X<0.5){
                    newb.type = 'normal_h';
                }else{
                    newb.type = 'normal';
                }
                game.physics.enable(newb);
                newb.enableBody = true;
                newb.rotation = enemy_t_body.rotation-Math.PI/2;
                newb.time = game.time.now;
                game.physics.arcade.velocityFromAngle(newb.angle, 200, newb.body.velocity);
                newb.rotation += Math.PI/2;
                game_bullets.push(newb);
                enemy_t_body.cooldown = game.time.now;
                }
        }else if(type == "normal_v"){
            //console.log(enemy_t_body.type);
            while(game.time.now>enemy_t_body.cooldown+2000){
                var newb = game.add.sprite(enemy_t_body.x,enemy_t_body.y,'bulletup');
                if(X<0.5){
                    newb.type = 'normal_v';
                }else{
                    newb.type = 'normal';
                }
                game.physics.enable(newb);
                newb.enableBody = true;
                newb.rotation = enemy_t_body.rotation-Math.PI/2;
                newb.time = game.time.now;
                game.physics.arcade.velocityFromAngle(newb.angle, 200, newb.body.velocity);
                newb.rotation += Math.PI/2;
                game_bullets.push(newb);
                enemy_t_body.cooldown = game.time.now;
                }
        }
    },
    firemap(element){
        var X = Math.random();
        while(game.time.now>element.cooldown+5000){
            if(X>0.7){
                var newr = game.add.sprite(element.x,element.y,'maprocket');
                newr.anchor.setTo(0.5,0.5);
                newr.info_x = this.player.x;
                newr.info_y = this.player.y;
                game.physics.enable(newr);
                newr.rotation-=Math.PI/2;
                newr.enableBody = true;
                newr.body.velocity.x = -500;
                newr.checkWorldBounds = true;
                newr.events.onOutOfBounds.add(this.rocketupdate, this);
                var newtail = game.add.sprite(newr.x+64,newr.y+8,'tail');
                game.physics.enable(newtail);
                newtail.scale.setTo(1,0.5);
                newtail.animations.add('fly', [0,0, 1,1, 2,2, 3,3, 4,4, 5,5, 6,6, 7,7, 8,8, 9,9, 10,10, 11,11, 12,12, 13,13, 14,14, 15,15, 16,16, 17,17], 400);
                newtail.animations.play('fly');
                newtail.enableBody = true;
                newtail.rotation-=Math.PI;
                newtail.body.velocity.x = -500;
                newtail.checkWorldBounds = true;
                newtail.events.onOutOfBounds.add(this.tailupdate, this);
                
            }
            element.cooldown=game.time.now;
        }
    },
    createPlayer:function(){
        this.player = game.add.sprite(game.width/2,0,'player');
        this.player.anchor.setTo(0.5, 0.5); // Set anchor to middle of sprite
        this.player.scale.setTo(1.5,1.5);   //Set scale
        game.physics.enable(this.player);    // Enable Physics
        this.player.enableBody = true;      // Enable Physics
        this.player.position.x=game.width/4;
        this.player.position.y=map_length-80;
        this.game.camera.follow(this.player);
        player_life = 10;
        last_life = 10;
    },
    createPlayer_alias:function(){
        player_alias = game.add.sprite(game.width/2,0,'player');
        player_alias.anchor.setTo(0.5, 0.5); // Set anchor to middle of sprite
        player_alias.scale.setTo(1.5,1.5);   //Set scale
        game.physics.enable(player_alias);    // Enable Physics
        player_alias.enableBody = true;      // Enable Physics
        player_alias.position.x=game.width*3/4;
        player_alias.position.y=map_length-80;
    },
    playerupdate:function(player) {
            if(keyboard.left.isDown) {
                if(keyboard.up.isDown){
                    player.body.velocity.x = -250;
                    player.body.velocity.y = -250;
                }
                else if(keyboard.down.isDown){
                    player.body.velocity.x = -250;
                    player.body.velocity.y = 250;
                }
                else{
                    player.body.velocity.x = -250;
                    player.body.velocity.y = 0;
                }	
            } else if(keyboard.right.isDown) {
                 if(keyboard.up.isDown){
                    player.body.velocity.x = 250;
                    player.body.velocity.y = -250;
                }
                else if(keyboard.down.isDown){
                    player.body.velocity.x = 250;
                    player.body.velocity.y = 250;
                }
                else{
                    player.body.velocity.x = 250;
                    player.body.velocity.y = 0;
                }
            } else if(keyboard.up.isDown) {
                player.body.velocity.y = -250;
                player.body.velocity.x = 0;
            } else if(keyboard.down.isDown) {
                player.body.velocity.y = 250;
                player.body.velocity.x = 0;
            } else {
                player.body.velocity.x = 0;
                player.body.velocity.y = 0;
            }

            if(this.player.y-player_alias.y>200){
                if(this.player.body.velocity.y > 0){
                    this.player.body.velocity.y = 0;
                }
                if(player_alias.body.velocity.y < 0){
                    player_alias.body.velocity.y = 0;
                }
            }else if(this.player.y-player_alias.y<-200){
                if(this.player.body.velocity.y < 0){
                    this.player.body.velocity.y = 0;
                }
                if(player_alias.body.velocity.y > 0){
                    player_alias.body.velocity.y = 0;
                }
            }
         
        //playeranimate(player);
    },
    playerfire:function(){
        console.log('yeH');

    },
    createHeart:function(){
        //this.hearts = game.add.group();
        //console.log(game.camera.position.y);
        this.heart1 = game.add.sprite(1208,  (map_length-700), '24heart');
        this.heart2 = game.add.sprite(1208, (map_length-700)+24*1, '24heart');
        this.heart3 = game.add.sprite(1208, (map_length-700)+24*2, '24heart');
        this.heart4 = game.add.sprite(1208, (map_length-700)+24*3, '24heart');
        this.heart5 = game.add.sprite(1208, (map_length-700)+24*4, '24heart');
        this.heart6 = game.add.sprite(1208, (map_length-700)+24*5, '24heart');
        this.heart7 = game.add.sprite(1208, (map_length-700)+24*6, '24heart');
        this.heart8 = game.add.sprite(1208, (map_length-700)+24*7, '24heart');
        this.heart9 = game.add.sprite(1208, (map_length-700)+24*8, '24heart');
        this.heart10 = game.add.sprite(1208, (map_length-700)+24*9, '24heart');
        this.heart1.animations.add('damage', [1, 1], 14);
        this.heart2.animations.add('damage', [1, 1], 14);
        this.heart3.animations.add('damage', [1, 1], 14);
        this.heart4.animations.add('damage', [1, 1], 14);
        this.heart5.animations.add('damage', [1, 1], 14);
        this.heart6.animations.add('damage', [1, 1], 14);
        this.heart7.animations.add('damage', [1, 1], 14);
        this.heart8.animations.add('damage', [1, 1], 14);
        this.heart9.animations.add('damage', [1, 1], 14);
        this.heart10.animations.add('damage', [1, 1], 14);
        this.heart1.animations.add('heal', [0, 0], 14);
        this.heart2.animations.add('heal', [0, 0], 14);
        this.heart3.animations.add('heal', [0, 0], 14);
        this.heart4.animations.add('heal', [0, 0], 14);
        this.heart5.animations.add('heal', [0, 0], 14);
        this.heart6.animations.add('heal', [0, 0], 14);
        this.heart7.animations.add('heal', [0, 0], 14);
        this.heart8.animations.add('heal', [0, 0], 14);
        this.heart9.animations.add('heal', [0, 0], 14);
        this.heart10.animations.add('heal', [0, 0], 14);
    },
    heartupdate:function(){
        this.heart1.position.y = game.camera.position.y; 
        this.heart2.position.y = game.camera.position.y+24*1; 
        this.heart3.position.y = game.camera.position.y+24*2; 
        this.heart4.position.y = game.camera.position.y+24*3; 
        this.heart5.position.y = game.camera.position.y+24*4; 
        this.heart6.position.y = game.camera.position.y+24*5; 
        this.heart7.position.y = game.camera.position.y+24*6; 
        this.heart8.position.y = game.camera.position.y+24*7; 
        this.heart9.position.y = game.camera.position.y+24*8; 
        this.heart10.position.y = game.camera.position.y+24*9; 
        if(last_life>player_life){
            last_life = player_life;
            if(player_life<=0){
                this.heart10.animations.play('damage');
                //console.log("in");
            }
            if(player_life<=1){
                this.heart9.animations.play('damage');
            }
            if(player_life<=2){
                this.heart8.animations.play('damage');
            }
            if(player_life<=3){
                this.heart7.animations.play('damage');
            }
            if(player_life<=4){
                this.heart6.animations.play('damage');
            }
            if(player_life<=5){
                this.heart5.animations.play('damage');
            }
            if(player_life<=6){
                this.heart4.animations.play('damage');
            }
            if(player_life<=7){
                this.heart3.animations.play('damage');
            }
            if(player_life<=8){
                this.heart2.animations.play('damage');
            }
            if(player_life<=9){
                this.heart1.animations.play('damage');
            }
        }else if(last_life<player_life){
            last_life = player_life;
            if(player_life>=10){
                this.heart1.animations.play('heal');
                //console.log("in");
            }
            if(player_life>=9){
                this.heart2.animations.play('heal');
            }
            if(player_life>=8){
                this.heart3.animations.play('heal');
            }
            if(player_life>=7){
                this.heart4.animations.play('heal');
            }
            if(player_life>=6){
                this.heart5.animations.play('heal');
            }
            if(player_life>=5){
                this.heart6.animations.play('heal');
            }
            if(player_life>=4){
                this.heart7.animations.play('heal');
            }
            if(player_life>=3){
                this.heart8.animations.play('heal');
            }
            if(player_life>=2){
                this.heart9.animations.play('heal');
            }
            if(player_life>=1){
                this.heart10.animations.play('heal');
            }
        }
    },
    step_on_mine:function(player,mine){
        //smoke emitter
        this.show_smoke(mine);
        //fire emitter
        this.show_explode(mine);
        
        player_life--;
        //console.log(player_life);
        mine.kill();
    },
    step_on_heal_pack:function(player,heal_pack){
        if(player_life<10){
            player_life++;
        }
        //console.log(player_life);
        heal_pack.kill();
    },
    enemy_tupdate:function(){
        //rotate turrets
        this.enemy_ts.forEach(element => {
            if(element.life<1){
                this.show_explode(element);
                this.show_smoke(element);
                element.kill;
            }
            element.rotation = Math.atan2(this.player.y - element.y, this.player.x - element.x)+Math.PI/2; 
            if(this.distance(element.x,element.y,this.player.x,this.player.y)<250){
                //console.log(this.distance(element.x,element.y,this.player.x,this.player.y));
                this.fireBullet(element,'rocket');
            }
        });
        //horizontal turrets
        this.enemy_hs.forEach(element => {
            //console.log(this.player.x+' '+element.x);
            if(this.player.y>element.y){
                element.angle = 180; 
            }else{
                element.angle = 0; 
            }
            if(this.player.x>element.position.x+40){
                element.body.velocity.x=200*Math.random();
            }else if(this.player.x<element.position.x-40){
                element.body.velocity.x=-200*Math.random();
            }
            if(this.distance(element.x,element.y,this.player.x,this.player.y)<500){
                this.fireBullet(element,'normal_h');
            }
            //console.log(element.body.velocity.x);
            game.physics.arcade.collide(element,this.layer);
        });
        //vertical turrets
        this.enemy_vs.forEach(element => {
            if(this.player.x>element.x){
                element.angle = 90; 
            }else{
                element.angle = 270; 
            }
            if(this.player.y>element.position.y+40){
                if(element.position.y-element.init<200){
                    element.body.velocity.y=200*Math.random();
                }
                else{
                    element.body.velocity.y=0;
                }
            }else if(this.player.y<element.position.y-40){
                if(element.position.y-element.init>-200){
                    element.body.velocity.y=-200*Math.random();
                }
                else{
                    element.body.velocity.y=0;
                }
            }
            if(this.distance(element.x,element.y,this.player.x,this.player.y)<500){
                this.fireBullet(element,'normal_v');
            }
            game.physics.arcade.collide(element,this.layer);
        });
        this.enemy_maps.forEach(element=>{
            if(this.distance(element.x,element.y,this.player.x,this.player.y)<700){
                this.firemap(element);
            }
        });
    },
    bulletupdate:function(){
        for(var i =0;i<game_bullets.length;i++){
            //console.log(game_bullets[i]);
            if(game_bullets[i].body){
                if(game_bullets[i].type=='rocket'){
                    //將火箭的方向轉向玩家
                    game_bullets[i].rotation =  Math.atan2(this.player.y - game_bullets[i].y, this.player.x - game_bullets[i].x); 
                    //用這個方向為速度方向
                    game.physics.arcade.velocityFromAngle(game_bullets[i].angle, 200, game_bullets[i].body.velocity);
                    //把火箭'圖'的方向轉回來
                    game_bullets[i].rotation+=Math.PI/2;
                    if((game.time.now-game_bullets[i].time)>rocket_lifetime){
                        this.show_explode(game_bullets[i]);
                        this.show_smoke(game_bullets[i]);    
                        game_bullets[i].destroy();
                        game_bullets.splice(i,1);
                    }
                }else if(game_bullets[i].type=='normal_h'){
                    //this.show_blood(game_bullets[i]);
                    
                    if(this.player.x>game_bullets[i].x+20){
                        game_bullets[i].body.velocity.x=Math.sqrt(20000);
                        game_bullets[i].body.velocity.y=(game_bullets[i].body.velocity.y>0)?Math.sqrt(20000):-Math.sqrt(20000);
                    }else if(this.player.x<game_bullets[i].x-20){
                        game_bullets[i].body.velocity.x=-Math.sqrt(20000);
                        game_bullets[i].body.velocity.y=(game_bullets[i].body.velocity.y>0)?Math.sqrt(20000):-Math.sqrt(20000);
                    }
                    
                    if((game.time.now-game_bullets[i].time)>bullet_lifetime){ 
                        this.show_little_smoke(game_bullets[i]);    
                        game_bullets[i].destroy();
                        game_bullets.splice(i,1);
                    }
                }else if(game_bullets[i].type=='normal_v'){
                    //this.show_blood(game_bullets[i]);
                    
                    if(this.player.y>game_bullets[i].y+20){
                        game_bullets[i].body.velocity.y=Math.sqrt(20000);
                        game_bullets[i].body.velocity.x=(game_bullets[i].body.velocity.x>0)?Math.sqrt(20000):-Math.sqrt(20000);
                    }else if(this.player.y<game_bullets[i].y-20){
                        game_bullets[i].body.velocity.y=-Math.sqrt(20000);
                        game_bullets[i].body.velocity.x=(game_bullets[i].body.velocity.x>0)?Math.sqrt(20000):-Math.sqrt(20000);
                    }
                    
                    if((game.time.now-game_bullets[i].time)>bullet_lifetime){
                        this.show_little_smoke(game_bullets[i]);    
                        game_bullets[i].destroy();
                        game_bullets.splice(i,1);
                    }
                }else if(game_bullets[i].type=='normal'){
                    if((game.time.now-game_bullets[i].time)>bullet_lifetime){
                        this.show_little_smoke(game_bullets[i]);    
                        game_bullets[i].destroy();
                        game_bullets.splice(i,1);
                    }
                }
                if(game.physics.arcade.collide(game_bullets[i],this.layer)){
                    //console.log("yes");
                    if(game_bullets[i].type=='rocket'){
                        this.show_explode(game_bullets[i]);
                        this.show_smoke(game_bullets[i]);
                    }else{
                        this.show_little_smoke(game_bullets[i]);    
                    } 
                    game_bullets[i].destroy();
                    game_bullets.splice(i,1);
                //}   
                }else if(game.physics.arcade.collide(game_bullets[i],this.player)){
                    if(game_bullets[i].type=='rocket'){
                        player_life--;
                        this.show_explode(game_bullets[i]);
                        this.show_smoke(game_bullets[i]); 
                    }else if(game_bullets[i].type=='normal_h'||game_bullets[i].type=='normal_v'||game_bullets[i].type=='normal'){
                        player_life--;
                        this.show_blood(game_bullets[i]);
                    } 
                    game_bullets[i].destroy();
                    game_bullets.splice(i,1);
                }else{

                }
            }
            
            
        }
            
        
    },
    rocketupdate(rocket){
        console.log('kill');
        var newtarget = game.add.sprite(rocket.info_x,rocket.info_y,'target');
        newtarget.anchor.setTo(0.5,0.5);
        newtarget.scale.setTo(0.2,0.2);
        newtarget.alpha = 0.2;
        newtarget.time = game.time.now;
        newtarget.count = 0;
        map_targets.push(newtarget);
        map_rockets.push(rocket);
    },
    tailupdate(tail){
        tail.kill();
    },
    rocket_to_drop(){
        for(var i = 0; i < map_rockets.length;i++){
            if(map_rockets[i]){
                map_rockets[i].rotation = Math.PI/2;
                map_rockets[i].y = map_rockets[i].info_y;
                if(map_rockets[i].x<map_rockets[i].info_x){
                    map_rockets[i].body.velocity.x = 300;
                }
                else{
                    map_rockets[i].body.velocity.x = 0;
                    this.show_explode(map_rockets[i]);
                    this.show_smoke(map_rockets[i]);
                    console.log(this.distance(map_rockets[i].x,map_rockets[i].y,this.player.x,this.player.y));
                    if(this.distance(map_rockets[i].x,map_rockets[i].y,this.player.x,this.player.y)<100){
                        player_life-=2;
                        console.log('life '+player_life);
                    }
                    map_rockets[i].destroy();
                    map_rockets.splice(i,1);
                }
            }
            
        }
    },
    target_to_alert(){
        for(var i = 0; i < map_targets.length;i++){
            if(map_targets[i]){
                if(game.time.now-map_targets[i].time>100){
                    map_targets[i].alpha *=1.5;
                    map_targets[i].time = game.time.now;
                    map_targets[i].count++;
                }
                if(map_targets[i].count>8){
                    map_targets[i].destroy();
                    map_targets.splice(i,1);
                }
            }
            
        }
            
    },
    collide(player, layer){
        game.physics.arcade.collide(player, layer);
    },
    collide_bullet(bullet,layer,i){
        
        bullet.destroy();
    },
    show_explode(element){
        exploding=game.add.emitter(element.position.x, element.position.y, 15);
        var x_randy_rand = 150*Math.random();
        var x_rand_y_rand = 150*Math.random();
        exploding.makeParticles('explode');
        exploding.setYSpeed(-(x_randy_rand)/2, (x_randy_rand)/2);
        exploding.setXSpeed(-(x_rand_y_rand)/2, (x_rand_y_rand)/2);
        exploding.setScale(2, 0, 2, 0, 800);
        exploding.gravity = 0;
        exploding.start(true,800, null, 15);
    },
    show_smoke(element){
        smoke_emit=game.add.emitter(element.position.x, element.position.y, 15);
        var x_rand = 150*Math.random();
        var y_rand = 150*Math.random();
        smoke_emit.makeParticles('smoke');
        smoke_emit.setYSpeed(-y_rand, y_rand);
        smoke_emit.setXSpeed(-x_rand, x_rand);
        smoke_emit.setScale(2, 0, 2, 0, 800);
        smoke_emit.gravity = 0;
        smoke_emit.start(true,800, null, 15);
    },
    show_little_smoke(element){
        smoke_emit=game.add.emitter(element.position.x, element.position.y, 15);
        var x_rand = 150*Math.random();
        var y_rand = 150*Math.random();
        smoke_emit.makeParticles('smoke');
        smoke_emit.setYSpeed(-y_rand, y_rand);
        smoke_emit.setXSpeed(-x_rand, x_rand);
        smoke_emit.setScale(0.5, 0, 0.5, 0, 800);
        smoke_emit.gravity = 0;
        smoke_emit.start(true,800, null, 15);
    },
    show_blood(element){
        blood_emit=game.add.emitter(element.position.x, element.position.y, 15);
        var x_rand = 150*Math.random();
        var y_rand = 150*Math.random();
        blood_emit.makeParticles('blood');
        blood_emit.setYSpeed(-y_rand, y_rand);
        blood_emit.setXSpeed(-x_rand, x_rand);
        blood_emit.setScale(2, 0, 2, 0, 800);
        blood_emit.gravity = 0;
        blood_emit.start(true,800, null, 15);
    },
    distance: function (x1, y1, x2, y2) {
        var dx = x1 - x2;
        var dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    },
    check_game_over:function(){
        if(player_life<=0){
            game.state.start('menu'); 
        }
    }
};
var SinglePlayerGameOverState = {
    preload:function(){

    },
    create:function(){
        game.state.start('menu');
    }
}
var player_alias;
var player_life;
var exploding;
var smoke_emit;
var game_bullets = [];
var map_rockets = [];
var map_targets = [];
var last_life = 10;
var rocket_lifetime=3000;
var bullet_lifetime = 5000;
var map_length = 3600;
